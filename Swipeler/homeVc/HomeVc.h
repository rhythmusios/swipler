//
//  HomeVc.h
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface HomeVc : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate>{
    
    NSMutableArray*menuItemsSection1;
    NSMutableArray *swipelerTitle ;
    NSMutableArray*txtDetails;
    NSString*strTitle ;
    NSMutableArray*menuItemsSection2;
    NSMutableArray*sectionImages1;
    NSMutableArray*sectionImages2;
    
    NSMutableArray*merchantTitleArray;
    BOOL checkMerchant;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;

@property (strong, nonatomic) IBOutlet UITableView *homeTableView;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSlide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewWidth;
- (IBAction)menuClicked:(id)sender;


@property (strong, nonatomic) IBOutlet UITableView *sideTableView;

@end
