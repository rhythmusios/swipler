//
//  HomeCell.m
//  CityBus
//
//  Created by katoch on 22/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
