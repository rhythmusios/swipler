//
//  HomeVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "HomeVc.h"
#import "HomeCell.h"
#import "AccountVc.h"
#import "SettingsVc.h"
#import "ProfileVc.h"
#import "NotificationVc.h"
#import "ChangePasswordVc.h"
#import "OffersVc.h"
#import "AppDelegate.h"
#import "loginVc.h"
#import "SwiplerUserVc.h"
#import "userListVc.h"
#import "sideMenuCell.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"




@interface HomeVc ()
{
    NSMutableArray * accountBalArray;
     NSArray * IDarray;
}
@end

@implementation HomeVc

- (void)viewDidLoad {
    [super viewDidLoad];
    //merchantTitleArray = [NSMutableArray alloc]
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width/2 ;
_imgProfile.layer.masksToBounds = YES;
    
    accountBalArray = [[NSMutableArray alloc]initWithObjects:@"₹ 400",@"₹ 500",@"10",@"₹ 1500.75", nil];
    IDarray = [[NSArray alloc]initWithObjects:@"ID 8457596215",@"",@"",@"", nil];
    
    [_sideTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"pink"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    _viewMenuSlide.hidden = true;
   menuItemsSection1 = [[NSMutableArray alloc]initWithObjects:@"Profile",@"Notification",@"Change Password",@"Promotions and Offers",nil];
      menuItemsSection2 = [[NSMutableArray alloc]initWithObjects:@"More",@"Settings",@"Logout",nil];
    
    sectionImages1 = [[NSMutableArray alloc]initWithObjects:@"name.png",@"notifications.png",@"pass.png",@"offers.png", nil];
     sectionImages2 = [[NSMutableArray alloc]initWithObjects:@"settings.png",@"settings.png",@"logout.png",nil];
    
    swipelerTitle = [[NSMutableArray alloc]initWithObjects:@"Swipeler Account",@"Swipeler Wallet",@"Swipeler User",@"Swipeler Dashboard", nil];
    
    txtDetails = [[NSMutableArray alloc]initWithObjects:@"Account Balance",@"Wallet Balance",@"User Details",@"Current Month Usages", nil];
    
    
    
//    [self.view bringSubviewToFront:_homeTableView];
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_homeTableView]) {
        
        
        
        return NO;
    }else if ([touch.view isDescendantOfView:_sideTableView]) {
        
        
        
        return NO;
    }
    
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if (tableView== _sideTableView) {
        
        return 2;
        
        
    }else{
        
        return 1;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView== _sideTableView) {
        
        if (section == 0){
        return menuItemsSection1.count ;
            
        }else{
            return menuItemsSection2.count ;
            
        }
    }
    
    else
    {

        return swipelerTitle.count;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _homeTableView) {
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 120;
        }else{
        return 145;
        }
    }
    else
    {
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            return 44;
        }else{
        return 60 ;
            
        }
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _homeTableView) {
        HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"HomeCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
 
        [cell.txtHeaderView.layer setShadowColor:[UIColor lightGrayColor].CGColor] ;
        [cell.txtHeaderView.layer setShadowOpacity:0.75];
        [cell.txtHeaderView.layer setShadowRadius:3.0];
        [cell.txtHeaderView.layer setShadowOffset:CGSizeMake(0, 5.0f)];

        


        cell.txtHeader.text = [swipelerTitle objectAtIndex:indexPath.row];
           cell.txtAccountName.text = [txtDetails objectAtIndex:indexPath.row];
        cell.txtBalance.text = [accountBalArray objectAtIndex:indexPath.row];
        cell.txtId.text = [IDarray objectAtIndex:indexPath.row];
      
        return cell;
        
        
    }else if (tableView== _sideTableView){
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        
        if (indexPath.section == 0) {
             cell.txtMenu.text  = [menuItemsSection1 objectAtIndex:indexPath.row];
            cell.menuImg.image = [UIImage imageNamed:[sectionImages1 objectAtIndex:indexPath.row]];
        }
        if (indexPath.section == 1) {
            
           
            cell.txtMenu.text  = [menuItemsSection2 objectAtIndex:indexPath.row];
             cell.menuImg.image = [UIImage imageNamed:[sectionImages2 objectAtIndex:indexPath.row]];
                cell.menuImg.hidden = NO;
            
            if (indexPath.row==0) {
                cell.menuImg.hidden = YES;
                
                cell.xMore.constant = 0 ;
                
                cell.txtMenu.font = [UIFont systemFontOfSize:14];
                
                [self.view layoutIfNeeded];
                
            }
            
        }
        
//        cell.textLabel.font = [UIFont systemFontOfSize:12];
        
        return cell;
        
        
    }
    return 0;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==_sideTableView) {
        
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                ProfileVc *PVc = [[ProfileVc alloc]init];
                PVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVc"];
                [self.navigationController pushViewController:PVc animated:YES];
            }else if (indexPath.row == 1) {
                NotificationVc *NVc = [[NotificationVc alloc]init];
                NVc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVc"];
                [self.navigationController pushViewController:NVc animated:YES];
            }
            else if (indexPath.row == 2) {
                ChangePasswordVc *CVc = [[ChangePasswordVc alloc]init];
                CVc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVc"];
                [self.navigationController pushViewController:CVc animated:YES];
            }
            else if (indexPath.row == 3) {
                OffersVc *OVc = [[OffersVc alloc]init];
                OVc = [self.storyboard instantiateViewControllerWithIdentifier:@"OffersVc"];
                [self.navigationController pushViewController:OVc animated:YES];
            }
        }else{
            
             if (indexPath.row == 1) {
                SettingsVc *SVc = [[SettingsVc alloc]init];
                SVc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVc"];
                [self.navigationController pushViewController:SVc animated:YES];
            }else if (indexPath.row== 2){
                
                UIAlertView*logout = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                logout.tag = 11 ;
                logout.delegate = self;
                [logout show];
                
            }
        }
       
    }else{
        if (indexPath.row == 0) {
            
            strTitle = @"Account";
            AccountVc *accVc = [[AccountVc alloc]init];
            accVc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountVc"];
            accVc.checkStr = strTitle ;
            [self.navigationController pushViewController:accVc animated:YES];
        }else if (indexPath.row == 1) {
            
             strTitle = @"Wallet";
            AccountVc *accVc = [[AccountVc alloc]init];
            accVc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountVc"];
            accVc.checkStr = strTitle ;
            [self.navigationController pushViewController:accVc animated:YES];
            
        }

        else  if (indexPath.row == 2) {
            
            userListVc *UVc = [[userListVc alloc]init];
            UVc = [self.storyboard instantiateViewControllerWithIdentifier:@"userListVc"];
            [self.navigationController pushViewController:UVc animated:YES];
            
        }

    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
     if (alertView.tag ==11){
        
        if (buttonIndex == 1) {
           
            AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            loginVc*lVc = [[loginVc alloc]init];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            lVc= [storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            UINavigationController *navControllr = [[UINavigationController alloc]initWithRootViewController:lVc];
            navControllr.navigationBarHidden = true;
            delegate.window.rootViewController= navControllr;
            [delegate.window makeKeyAndVisible];
            
            
        }
    }
    
}


- (IBAction)menuClicked:(id)sender {
    
//     [self.view sendSubviewToBack:_homeTableView];
    
     _viewMenuSlide.hidden = false;
        
        [UIView animateWithDuration:0.5 animations:^{
            _viewX.constant = 0;
            _viewWidth.constant = self.view.frame.size.width - 50 ;

            [self.view layoutIfNeeded];
            _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
            
        } completion:^(BOOL finished) {
//            _viewMenuSlide.hidden = true;
            
        }];
    
}

- (IBAction)backClicked:(id)sender {
    
    
}

-(void)tapGesture{
    
    [UIView animateWithDuration:0.5 animations:^{
        _viewX.constant = -self.view.frame.size.width;
        _viewWidth.constant = self.view.frame.size.width - 50 ;
        
        [self.view layoutIfNeeded];
        _viewMenuSlide.backgroundColor = [UIColor colorWithRed:0.0f/255 green:0.0f/255 blue:0.0f/255 alpha:0.5];
        
    } completion:^(BOOL finished) {
                    _viewMenuSlide.hidden = true;
        
    }];
    
}


-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 2.0;
    }else{
        
        return 0.0 ;
    }
    
}



@end
