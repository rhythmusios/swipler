//
//  HomeCell.h
//  CityBus
//
//  Created by katoch on 22/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *txtHeader;
@property (strong, nonatomic) IBOutlet UILabel *txtId;
@property (strong, nonatomic) IBOutlet UILabel *txtBalance;
@property (strong, nonatomic) IBOutlet UILabel *txtAccountName;
@property (strong, nonatomic) IBOutlet UIView *txtHeaderView;

@end
