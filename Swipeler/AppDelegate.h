//
//  AppDelegate.h
//  Swipeler
//
//  Created by katoch on 19/01/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreTelephony/CTCall.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    id versionCheck ;
    
    
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

