//
//  AddMoneyVc.m
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "AddMoneyVc.h"
#import "KVNProgress.h"
#import <AFNetworking/AFNetworking.h>

@interface AddMoneyVc ()

@end

@implementation AddMoneyVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.btnAddMoney.layer.cornerRadius = _btnAddMoney.frame.size.height/2 ;
     self.btnCancel.layer.cornerRadius = _btnCancel.frame.size.height/2 ;
     self.btnHundred.layer.cornerRadius = _btnHundred.frame.size.height/2 ;
     self.btnTwoHundred.layer.cornerRadius = _btnTwoHundred.frame.size.height/2 ;
     self.btnFiveHundred.layer.cornerRadius = _btnFiveHundred.frame.size.height/2 ;
     self.btnThousand.layer.cornerRadius = _btnThousand.frame.size.height/2 ;
   
    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *passwordImage = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, 15, 15)];
    [passwordImage setImage:[UIImage imageNamed:@"india-rupee.png"]];
    [passwordPadding addSubview:passwordImage];
    _txtAmount.leftViewMode = UITextFieldViewModeAlways;
    _txtAmount.leftView = passwordPadding ;
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (IBAction)hundredClicked:(id)sender {
    
    if ([_txtAmount.text isEqualToString:@""]) {
        _txtAmount.text = @"100" ;
    }else{
        
        NSString* amount = _txtAmount.text ;
         finalamount = [amount doubleValue] + 100 ;
        

        _txtAmount.text =  [NSString stringWithFormat:@"%.02f",finalamount];
        
        
    }
    
}

- (IBAction)twoHundredClicked:(id)sender {
    
    if ([_txtAmount.text isEqualToString:@""]) {
        _txtAmount.text = @"200" ;
    }else{
        
        NSString* amount = _txtAmount.text ;
         finalamount = [amount doubleValue] + 200 ;
        
        
        _txtAmount.text =  [NSString stringWithFormat:@"%.02f",finalamount];
        
        
    }
}

- (IBAction)fiveHundred:(id)sender {
    
    
    if ([_txtAmount.text isEqualToString:@""]) {
        _txtAmount.text = @"500" ;
    }else{
        
        NSString* amount = _txtAmount.text ;
         finalamount = [amount doubleValue] + 500 ;
        
        
        _txtAmount.text =  [NSString stringWithFormat:@"%.02f",finalamount];
        
        
    }
}

- (IBAction)thousandClicked:(id)sender {
    if ([_txtAmount.text isEqualToString:@""]) {
        _txtAmount.text = @"1000" ;
    }else{
        
        NSString* amount = _txtAmount.text ;
         finalamount = [amount doubleValue] + 1000 ;
        _txtAmount.text =  [NSString stringWithFormat:@"%.02f",finalamount];
        
        
    }
}



- (IBAction)addMoneyClicked:(id)sender {
    [KVNProgress show];
    [self rechargeApiPostToServer];

}

- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}

-(void)tapGesture{
    [self.view endEditing:YES];
}


-(void)rechargeApiPostToServer{
    
    consumerId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/rechrg/order"];
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSDictionary* params = [[NSDictionary alloc] init];
    
    NSString* amountAdded = _txtAmount.text ;
    NSInteger addedValue = [amountAdded doubleValue];
    
    params = @{
               
               @"consumerId" :consumerId,
               @"amt" : [NSString stringWithFormat:@"%ld",addedValue]
               
               
                   };
    
    
    [managerr POST:apiURLStr parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        rechargeData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(RechargeMessage) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        
        
    }];
    
    
}
-(void)RechargeMessage{
    
    if ([[rechargeData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        
        [self PaymenyApiPostToServer];
        
       
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[rechargeData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
    
}
-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}



-(void)PaymenyApiPostToServer{
    
   
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/rechrg/pay"];
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSDictionary* params = [[NSDictionary alloc] init];
   
    params = @{
               @"ordrId" : [rechargeData valueForKey:@"ordrId"],
               @"consumerId" :consumerId,
               @"paySts" :@"Successful",
                @"trancationId": @"8BL15100CF123456D"
               
               };
    
    
    [managerr POST:apiURLStr parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        paymentData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(paymentMessage) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error from server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }];
    
    
}
-(void)paymentMessage{
    
    if ([[rechargeData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        
        NSString*amountStr = _txtAmount.text ;
        
       
//        [_delegate sendDataToA:amountStr];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[rechargeData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
    
}


- (BOOL)isAlphanumeric:(NSString *)string {
    return [string rangeOfString:@"^[a-zA-Z0-9]+$" options:NSRegularExpressionSearch].location != NSNotFound;
}

@end
