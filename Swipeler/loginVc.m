//
//  loginVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "loginVc.h"
#import "ForgetPasswordVc.h"
#import "HomeVc.h"
#import <QuartzCore/QuartzCore.h>
#import "OtpVc.h"
#import <AFNetworking/AFNetworking.h>
#import "registerVc.h"
#import "HomeVc.h"
#import "KVNProgress.h"




@interface loginVc ()

@end

@implementation loginVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    [dropImage setImage:[UIImage imageNamed:@"drop-down-arrow.png"]];
    [dropDownPadding addSubview:dropImage];
    _userType.rightViewMode = UITextFieldViewModeAlways;
    _userType.rightView = dropDownPadding ;
    
    _userType.text = @"Individual";
     [_userType addTarget:self action:@selector(userTypeDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
     [self.firstImage setAlpha:1.0];
 dropDownArray = [[NSMutableArray alloc]initWithObjects:@"Individual",@"Retailer",@"Corporate", nil];
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    self.btnLogin.layer.cornerRadius = _btnLogin.frame.size.height/2 ;
     UIColor *color = [UIColor colorWithRed:255/255.0f green:200/255.0f blue:200/255.0f alpha:1];
    _txtEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile/Email/Swipeler ID" attributes:@{NSForegroundColorAttributeName: color}];
    _userType.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Individual" attributes:@{NSForegroundColorAttributeName: color}];
    _txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"pink"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        return NO;
    }
    
    
    return YES;
}
-(void)tapGesture{
    
    [_txtPassword resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_userType resignFirstResponder];
    
    [dropDownView closeAnimation];
    
}



- (IBAction)loginClicked:(id)sender {
    
//    HomeVc *home = [[HomeVc alloc]init];
//    home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVc"];
//    [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    [self.navigationController pushViewController:home animated:YES];
    
    
    if ([_txtEmail.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter Mobile/Email ID/Swipeler ID"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
     else  if ([self.userType.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please select the UserType"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if ([self.txtPassword.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }

    else{

        [KVNProgress show];
        [self loginApiToServer];
    }
    
}

- (IBAction)registerClicked:(id)sender {
    
    registerVc *SignUp = [[registerVc alloc]init];
    SignUp = [self.storyboard instantiateViewControllerWithIdentifier:@"registerVc"];
    [self.navigationController pushViewController:SignUp animated:YES];
    
}

- (IBAction)forgetClicked:(id)sender {
    ForgetPasswordVc *ForgetPassword = [[ForgetPasswordVc alloc]init];
    ForgetPassword = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPassword"];
    [self.navigationController pushViewController:ForgetPassword animated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    

    
//    [UIView animateWithDuration:0.8
//                          delay:0.1
//                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
//                     animations:^(void)
//     {
//         [_firstImage setAlpha:0.0];
//
//
//     }
//                     completion:^(BOOL finished)
//     {
//         if(finished)
//         {
//
//         }
//
//     }];
    
    if (textField == _userType) {
        [dropDownView.view removeFromSuperview];
                [self.txtEmail resignFirstResponder];
        [self.view endEditing:YES];
        
        
        dropDownView = [[DropDownView alloc] initWithArrayData:dropDownArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
        
       
        
        dropDownView.delegate = self;
        dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
        
        [self.viewLogin addSubview:dropDownView.view];
        
        [self.viewLogin bringSubviewToFront:dropDownView.view];
        
        _dropDownTxtfield = _userType ;
        
        [ dropDownView openAnimation];
        return NO ;
        
        
    }else
    {
         return YES;
    }
    
   
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [_txtEmail resignFirstResponder];
     [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
   
    [textField resignFirstResponder];
    return YES ;
    
}


-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    if (_dropDownTxtfield == _userType) {
        
        _userType.text = [dropDownArray objectAtIndex:returnIndex];
        
        if (_userType.text.length >0) {
            _userType.placeholder = @"UserType";
            
        }else{
            _userType.placeholder = @"Individual";
        }
        
        
        
        if ([_userType.text isEqualToString:@"Consumer"]) {
            userType = @"CNSMR_TYPE" ;
        }else if ([_userType.text isEqualToString:@"Merchant"]) {
            userType = @"MRCHNT_TYPE" ;
        }
        
    }
    
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     if (textField == _txtPassword) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
     }
//     else if (textField == _txtEmail) {
//         if (textField.text.length == 10 && range.length == 0)
//         {
//             return NO; // return NO to not change text
//         }
//         else
//         {
//             return YES;
//
//         }
//     }
    
    return YES;
    
}

-(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}



-(void)loginApiToServer{
    
    NSString* strPassword= self.txtPassword.text;
//    NSData *dataPass = [strPassword dataUsingEncoding:NSUTF8StringEncoding];
//
//    // Convert to Base64 data
//    NSData *base64Data = [dataPass base64EncodedDataWithOptions:0];
//    NSString* Base64encodePasswor = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:[base64Data bytes]]];
//    NSLog(@"%@", Base64encodePasswor);
    
    NSData *nsdata = [strPassword
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);
    
    // Let's go the other way...
    
    // NSData from the Base64 encoded str
     NSData *nsdataFromBase64String = [[NSData alloc]
                                      initWithBase64EncodedString:base64Encoded options:0];
    
    // Decoded NSString from the NSData
    NSString *base64Decoded = [[NSString alloc]
                               initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
    NSLog(@"Decoded: %@", base64Decoded);
    
    
        NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb//unsecure/lgn"];
        AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
        managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString*consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"CosumerId"];
    
    
    
        NSDictionary* _params = [[NSDictionary alloc] init];
        
//        [_params setObject:_txtEmail.text  forKey:@"mobile"];
//     [_params setObject:self.userType.text forKey:@"usrTyp"];
//        [_params setObject:self.txtPassword.text forKey:@"pswrd"];
    
    
    _params = @{
                
                @"mobile" :_txtEmail.text,
                @"usrTyp" :@"Individual",
                @"pswrd" :base64Encoded,
               
                };
    
        
        [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
         
         {
             NSLog(@"PLIST: %@", responseObject);
             [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
             
             loginData = responseObject;
             [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
             
             
         }
               failure:^(NSURLSessionTask *operation, NSError *error) {
             [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
             NSLog(@"Error: %@", error);
             
             
             
         }];
        
        
        
    }


-(void)checkResponse{
        
        if ([[loginData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
            HomeVc *home = [[HomeVc alloc]init];
            home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVc"];
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self.navigationController pushViewController:home animated:YES];
            
        }
        else{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:[loginData valueForKey:@"message"]
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
}

    
-(void)kvnDismiss{
        
        [KVNProgress dismiss];
    }
    
-(void)kvnStart{
        
        [KVNProgress show];
    }



@end
