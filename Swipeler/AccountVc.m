//
//  AccountVc.m
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "AccountVc.h"
#import "AccountsCell.h"
#import "AddMoneyVc.h"
#import "PPTransferVc.h"
#import "KVNProgress.h"
#import <AFNetworking/AFNetworking.h>

@interface AccountVc ()

@end

@implementation AccountVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnRecharge.layer.cornerRadius = _btnRecharge.frame.size.height/2 ;
    
    if ([_checkStr isEqualToString:@"Account"]) {
        
        _txtAccountType.text = @"Account Balance";
         [_btnRecharge setTitle:@"Recharge" forState:UIControlStateNormal];
        
    }else{
         _txtAccountType.text = @"Account Wallet";
        [_btnRecharge setTitle:@"Peer To Peer Transfer" forState:UIControlStateNormal];
    }
    
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self WalletApiPostToServer];
     [self transectionHistoryGetFromServer];
    
}



//-(void)sendDataToA:(NSString *)amountStr
//{
//    
//    NSString*amount = _txtBalance.text;
//    NSString*previousStr = [amount stringByReplacingOccurrencesOfString:@"₹" withString:@""];
//    
//    float previousAmount = [previousStr intValue];
//    float amountAdded = [amountStr intValue];
//    float totalBalance = previousAmount + amountAdded ;
//    
//    NSString*balanceStr = [NSString stringWithFormat:@"₹%.2f",totalBalance] ;
//    
//    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
//    CGSize expectedLabelSize = [balanceStr sizeWithFont:_txtBalance.font constrainedToSize:maximumLabelSize lineBreakMode:_txtBalance.lineBreakMode];
//    CGRect newFrame = _txtBalance.frame;
//    newFrame.size.width = expectedLabelSize.width;
//    _txtBalance.frame = newFrame;
//    _txtBalance.text = balanceStr ;
//
//    
//    
//    CGSize LabelSize = [balanceStr sizeWithFont:_txtAccountBalance.font constrainedToSize:maximumLabelSize lineBreakMode:_txtAccountBalance.lineBreakMode];
//    CGRect Frame = _txtAccountBalance.frame;
//    Frame.size.width = LabelSize.width;
//    _txtAccountBalance.frame = Frame;
//    _txtAccountBalance.text = balanceStr ;
//
//    
//    
//}


- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)txtAuthenticationLimit:(id)sender {
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    NSMutableArray *array = [TransectionId valueForKey:@"hstry"] ;
    
    
    if ([[TransectionId valueForKey:@"hstry"] count] > 0) {
        return [[TransectionId valueForKey:@"hstry"] count] ;
    }
    else{
        
        return 0 ;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _accountsTableView) {
        AccountsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountsCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"AccountsCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
       
        
        cell.txtAmount.text = [NSString stringWithFormat:@"₹%@",[[[TransectionId valueForKey:@"hstry"]valueForKey:@"amount"]objectAtIndex:indexPath.row]];

        
        cell.txtTransId.text =  [NSString stringWithFormat:@"%@",[[[TransectionId valueForKey:@"hstry"]valueForKey:@"trancationId"]objectAtIndex:indexPath.row]];
        
        
        NSString*strDate = [NSString stringWithFormat:@"%@",[[[TransectionId valueForKey:@"hstry"]valueForKey:@"creationDate"]objectAtIndex:indexPath.row]];
        
        double dateValue = [strDate doubleValue];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(dateValue / 1000.0)];
        
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"MM-dd-yyyy"]; // Date formater
        NSString *datestr = [dateformate stringFromDate:date]; // Convert date to string
        NSLog(@"date :%@",date);
        
        cell.txtDate.text = datestr ;
        
        cell.txtName.text = [TransectionId valueForKey:@"cnsmrName"];
        
        
        return cell;
        
        
    }
    return 0;
    
}


- (IBAction)rechargeClicked:(id)sender {
    if ([_btnRecharge.titleLabel.text isEqualToString:@"Peer To Peer Transfer"]) {
         PPTransferVc*PPVc = [[PPTransferVc alloc]init];
        PPVc = [self.storyboard instantiateViewControllerWithIdentifier:@"PPTransferVc"];
        [self.navigationController pushViewController:PPVc animated:YES];
    }else{
    
    AddMoneyVc *AddVc = [[AddMoneyVc alloc]init];
    AddVc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMoneyVc"];
        AddVc.delegate = self;
    [self.navigationController pushViewController:AddVc animated:YES];
        
    }
    
}


-(void)WalletApiPostToServer{
    
    consumerId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/wlt/%@",consumerId];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        walletData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(walletMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)walletMessage
{
    
    if ([[walletData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        balnceAmount =  [NSString stringWithFormat:@"₹%@",[walletData valueForKey:@"blncAmt"]];
        lastRecharge = [NSString stringWithFormat:@"₹%@",[walletData valueForKey:@"lstRchrg"]];
        lstWthdrw = [NSString stringWithFormat:@"₹%@",[walletData valueForKey:@"lstWthdrw"]];
        
        _txtBalance.text = balnceAmount ;
        _txtAccountBalance.text = balnceAmount ;
        
        _txtLastSpent.text = lastRecharge ;
        
    }
    else
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[walletData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)kvnDismiss{
    
    [KVNProgress dismiss];
    
}



-(void)transectionHistoryGetFromServer{
    
    consumerId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/rchrg/hstry/%@",consumerId];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        TransectionId = responseObject ;
        
        [self performSelectorOnMainThread:@selector(TransectionMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}

-(void)TransectionMessage
{
    
    if ([[TransectionId valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        if ([[TransectionId valueForKey:@"message"] isEqualToString:@"No Transaction"]) {
            
        }else{
            [_accountsTableView reloadData];
        }

        
    }
    else
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[TransectionId valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

@end
