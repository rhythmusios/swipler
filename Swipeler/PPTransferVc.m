//
//  PPTransferVc.m
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "PPTransferVc.h"
#import <QuartzCore/QuartzCore.h>

@interface PPTransferVc ()

@end

@implementation PPTransferVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.btnCancel.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    [self.viewAlertPayment setHidden:YES];
    self.viewAlertPayment.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.8];
      self.btnProceed.layer.cornerRadius = _btnProceed.frame.size.height/2 ;
      self.btnPayment.layer.cornerRadius = _btnPayment.frame.size.height/2 ;
     self.btnCancel.layer.cornerRadius = _btnCancel.frame.size.height/2 ;
    
//    _btnCancel.layer.borderWidth = 1.0f;
//    _btnCancel.layer.borderColor = [UIColor colorWithRed:245/255.0f green:115/255.0f blue:115/255.0f alpha:1].CGColor;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.viewSubAlert.bounds];
    self.viewSubAlert.layer.masksToBounds = NO;
    self.viewSubAlert.layer.cornerRadius = 7.0f;
    self.viewSubAlert.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.viewSubAlert.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.viewSubAlert.layer.shadowOpacity = 0.2f;
    self.viewSubAlert.layer.shadowPath = shadowPath.CGPath;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}



- (IBAction)cancelClicked:(id)sender {
    
     [self.viewAlertPayment setHidden:YES];
}

- (IBAction)txtProceed:(id)sender {
    
    [self.view bringSubviewToFront:_viewAlertPayment];
    [self.viewAlertPayment setHidden:NO];
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)proceedPaymentClicked:(id)sender {
    
}

-(void)tapGesture{
    [self.view endEditing:YES];
}

@end
