//
//  registerVc.h
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import "UIImageView+WebCache.h"
#import <AFNetworking/AFNetworking.h>

@interface registerVc : UIViewController<DropDownViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>{
    
    NSMutableArray*dropDownArray;
    DropDownView *dropDownView ;
    NSString *userType ;

    id countryData;
    NSMutableArray*countryList ;
     UIImageView *countryImage;
    NSMutableArray*countryArray;
    NSString*cuCode;
    NSInteger dtvalue ;
    
    NSMutableArray *merchantArray;
    NSMutableArray*businessCategory;
    NSMutableArray*SubCategory;
    NSMutableArray*proprietorArray;
    
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblBusinessDetailsTitle;

 @property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property (strong, nonatomic) IBOutlet UITextField *txtcountry;
@property (strong, nonatomic) UITextField *dropDownTxtfield;

@property (strong, nonatomic) IBOutlet UITextField *txtfirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtDOB;
@property (strong, nonatomic) IBOutlet UITextField *txtIndividual;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtGmail;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *countryTableView;
@property (strong, nonatomic) IBOutlet UIView *countryView;
@property (strong, nonatomic) IBOutlet UILabel *lblMobile;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblFirst;
@property (strong, nonatomic) IBOutlet UILabel *lblMiddle;
@property (strong, nonatomic) IBOutlet UILabel *lblLast;

////////merchant Type
@property (strong, nonatomic) IBOutlet UITextField *txtFirm;
@property (strong, nonatomic) IBOutlet UILabel *lblFirm;

@property (strong, nonatomic) IBOutlet UITextField *txtMerchantType;
@property (strong, nonatomic) IBOutlet UILabel *lblMerchant;


@property (strong, nonatomic) IBOutlet UITextField *txtBusinessMobile;
@property (strong, nonatomic) IBOutlet UILabel *lblBusinessMobile;

@property (strong, nonatomic) IBOutlet UITextField *txtProprietor;
@property (strong, nonatomic) IBOutlet UILabel *lblProprietor;

@property (strong, nonatomic) IBOutlet UITextField *txtBusinessCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblBCategory;
@property (strong, nonatomic) IBOutlet UIButton *btnMerchantNext;
- (IBAction)merchentNextClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtSubCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSubCategory;
///////////////merchant Ended//////

- (IBAction)loginClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)dateDoneClicked:(id)sender;
- (IBAction)backCountryClciked:(id)sender;


@end
