//
//  registerVc.m
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "registerVc.h"
#import "AadharVc.h"
#import "PassportVc.h"
#import "RPFloatingPlaceholderTextField.h"
#import "CountryCell.h"
#import "KVNProgress.h"

@interface registerVc ()

@end

@implementation registerVc

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self.searchBar setReturnKeyType:NO];
    
    [self.searchBar setBackgroundImage:[[UIImage alloc]init]];
[_searchBar setTintColor:[UIColor whiteColor]];
    
    [self.countryView setHidden:YES];

    _txtcountry.text = @"India";
    [_txtMobile addTarget:self action:@selector(MobileDidChange:) forControlEvents:UIControlEventEditingChanged];
  
     [_txtcountry addTarget:self action:@selector(countryDidChange:) forControlEvents:UIControlEventEditingChanged];
      [_txtGmail addTarget:self action:@selector(emailDidChange:) forControlEvents:UIControlEventEditingChanged];

    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    dropDownArray = [[NSMutableArray alloc]initWithObjects:@"Individual",@"Retailer",@"Corporate", nil];
    
     merchantArray = [[NSMutableArray alloc]initWithObjects:@"Retailer",@"Corporate", nil];
     businessCategory = [[NSMutableArray alloc]initWithObjects:@"Education",@"Telecom",@"Corporate", nil];
     SubCategory = [[NSMutableArray alloc]initWithObjects:@"Digital Art",@"Music Store",@"Audio Book", nil];
     proprietorArray = [[NSMutableArray alloc]initWithObjects:@"Private Ltd Company",@"Public Ltd Company",@"Sole proprietorship",@"Joint Hindu Family business",@"Partnership",@"Cooperatives",@"Limited Liability Partnership(LLP)",@"Subsidiary Company", nil];
    
    [self textfieldsPadding];
    self.btnNext.layer.cornerRadius = _btnNext.frame.size.height/2 ;
      self.btnMerchantNext.layer.cornerRadius = _btnMerchantNext.frame.size.height/2 ;

   _txtfirstName.adjustsFontSizeToFitWidth = YES;
    _txtMiddleName.adjustsFontSizeToFitWidth = YES;
    _txtLastName.adjustsFontSizeToFitWidth = YES;
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    _datePicker.maximumDate=[NSDate date];
    //    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    //    NSDate * currentDate = [NSDate date];
    //    NSDateComponents * comps = [[NSDateComponents alloc] init];
    //    [comps setYear: -12];
    //    NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    //    [comps setYear: -100];
    //    NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    //
    //
    //    self.datePicker.minimumDate = minDate;
    //    self.datePicker.maximumDate = maxDate;
    //    self.datePicker.date = maxDate;
    
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"pink"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    countryArray = [[NSMutableArray alloc]init];
    
//      [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
   [self CountryApiFromServer];
    
    

}

-(void)kvnStart{
    
    [KVNProgress show];
    
}

-(void)tapGesture{
    [dropDownView closeAnimation];
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }else if ([touch.view isDescendantOfView:_countryTableView]) {
        return NO;
    }
    return YES;
    
}
    


- (IBAction)loginClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextClicked:(id)sender {
    
    if ([self.txtcountry.text isEqualToString:@""]) {

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please select your Country"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {

                                   }];

        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }

   else if ([self.txtDOB.text isEqualToString:@""]) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please select date of birth"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    
    else if ([self.txtfirstName.text isEqualToString:@""]||self.txtfirstName.text.length < 3) {
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"First name should be of minimum 3 characters"
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        else if ([self.txtMobile.text isEqualToString:@""] || self.txtMobile.text.length != 10) {
            
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:nil
                                         message:@"Please enter the 10 digit mobile number"
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                       }];
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else if (self.txtGmail.text.length > 0) {
            
            if (![self emailStringIsValidEmail :self.txtGmail.text]){
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:nil
                                             message:@"Please enter the valid email address"
                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               
                                           }];
                
                [alert addAction:noButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }else{
                
                [self pushTonextView];
                
            }
            
        }
        
        else{
            
           
            [self pushTonextView];
            
            
        }
        
    }
    
   
    


-(void)pushTonextView{
    if ([_txtcountry.text isEqualToString:@"India"]) {
        
        
        AadharVc *Cvc = [[AadharVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"AadharVc"];
        Cvc.countryName = _txtcountry.text;
        Cvc.countryCode = [NSString stringWithFormat:@"%@",cuCode];
        Cvc.firstName = _txtfirstName.text;
        Cvc.lastName =  _txtLastName.text;
        Cvc.middleName = _txtMiddleName.text;
        Cvc.mobile = _txtMobile.text;
        Cvc.gmail = _txtGmail.text;
        Cvc.userType = _txtIndividual.text;
        Cvc.DOB = [NSString stringWithFormat:@"%ld",dtvalue];
        
        
        [self.navigationController pushViewController:Cvc animated:YES];
        
        
        
    }else{
        
        PassportVc *Cvc = [[PassportVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"PassportVc"];
        Cvc.countryName = _txtcountry.text;
        Cvc.countryCode = [NSString stringWithFormat:@"%@",cuCode];
        Cvc.firstName = _txtfirstName.text;
        Cvc.lastName =  _txtLastName.text;
        Cvc.middleName = _txtMiddleName.text;
        Cvc.mobile = _txtMobile.text;
        Cvc.gmail = _txtGmail.text;
        Cvc.userType = _txtIndividual.text;
        Cvc.DOB = [NSString stringWithFormat:@"%ld",dtvalue];
        [self.navigationController pushViewController:Cvc animated:YES];
        
    }
}

- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}



-(void)textfieldsPadding{
    
    UIView *userTypePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    countryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [countryImage setImage:[UIImage imageNamed:@"india.png"]];
    [userTypePadding addSubview:countryImage];
    _txtcountry.leftViewMode = UITextFieldViewModeAlways;
    _txtcountry.leftView = userTypePadding ;
    UIView *userPadding;
    UIImageView *image;
    if ([UIScreen mainScreen].bounds.size.width==320) {
       userPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
       image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _txtLastName.font = [UIFont systemFontOfSize:12];
        _txtfirstName.font = [UIFont systemFontOfSize:12];
        _txtMiddleName.font = [UIFont systemFontOfSize:12];
        
    }else{
 userPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        _txtLastName.font = [UIFont systemFontOfSize:14];
        _txtfirstName.font = [UIFont systemFontOfSize:14];
        _txtMiddleName.font = [UIFont systemFontOfSize:14];
        
    }
    [image setImage:[UIImage imageNamed:@"name.png"]];
    [userPadding addSubview:image];
    _txtfirstName.leftViewMode = UITextFieldViewModeAlways;
    _txtfirstName.leftView = userPadding ;
    
    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [dropImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    [dropDownPadding addSubview:dropImage];
    _txtcountry.rightViewMode = UITextFieldViewModeAlways;
    _txtcountry.rightView = dropDownPadding ;
    
    
    
    UIView *IndividualPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *IndividualImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [IndividualImage setImage:[UIImage imageNamed:@"name.png"]];
    [IndividualPadding addSubview:IndividualImage];
    _txtIndividual.leftViewMode = UITextFieldViewModeAlways;
    _txtIndividual.leftView = IndividualPadding ;
    
    
    UIView *dropPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [dropimg setImage:[UIImage imageNamed:@"dropDown.png"]];
    [dropPadding addSubview:dropimg];
    _txtIndividual.rightViewMode = UITextFieldViewModeAlways;
    _txtIndividual.rightView = dropPadding;
    
    
    UIView *mobilePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *mobileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [mobileImage setImage:[UIImage imageNamed:@"mobile.png"]];
    [mobilePadding addSubview:mobileImage];
    _txtMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtMobile.leftView = mobilePadding ;
    
    UIView *emailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *emailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [emailImage setImage:[UIImage imageNamed:@"email.png"]];
    [emailPadding addSubview:emailImage];
    _txtGmail.leftViewMode = UITextFieldViewModeAlways;
    _txtGmail.leftView = emailPadding ;
    
    UIView *aadharPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *aadharimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [aadharimage setImage:[UIImage imageNamed:@"calender.png"]];
    [aadharPadding addSubview:aadharimage];
    _txtDOB.leftViewMode = UITextFieldViewModeAlways;
    _txtDOB.leftView = aadharPadding ;
    
    UIView *firmPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *firmImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [firmImage setImage:[UIImage imageNamed:@"firm.png"]];
    [firmPadding addSubview:firmImage];
    _txtFirm.leftViewMode = UITextFieldViewModeAlways;
    _txtFirm.leftView = firmPadding ;
    
    
    UIView *merchantPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *merchantImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [merchantImage setImage:[UIImage imageNamed:@"name.png"]];
    [merchantPadding addSubview:merchantImage];
    _txtMerchantType.leftViewMode = UITextFieldViewModeAlways;
    _txtMerchantType.leftView = merchantPadding ;
    UIView *mDropPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *mdropimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [mdropimg setImage:[UIImage imageNamed:@"dropDown.png"]];
    [mDropPadding addSubview:mdropimg];
    _txtMerchantType.rightViewMode = UITextFieldViewModeAlways;
    _txtMerchantType.rightView = mDropPadding;
    
    
    
    UIView *buisnessPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *businessImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [businessImage setImage:[UIImage imageNamed:@"mobile.png"]];
    [buisnessPadding addSubview:businessImage];
    _txtBusinessMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtBusinessMobile.leftView = buisnessPadding ;

    UIView *proprietorPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *proprietorImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [proprietorImage setImage:[UIImage imageNamed:@"business.png"]];
    [proprietorPadding addSubview:proprietorImage];
    _txtProprietor.leftViewMode = UITextFieldViewModeAlways;
    _txtProprietor.leftView = proprietorPadding ;
    UIView *pDropPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *pdropimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [pdropimg setImage:[UIImage imageNamed:@"dropDown.png"]];
    [pDropPadding addSubview:pdropimg];
    _txtProprietor.rightViewMode = UITextFieldViewModeAlways;
    _txtProprietor.rightView = pDropPadding;
    
    
    UIView *categoryPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *categoryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [categoryImage setImage:[UIImage imageNamed:@"business.png"]];
    [categoryPadding addSubview:categoryImage];
    _txtBusinessCategory.leftViewMode = UITextFieldViewModeAlways;
    _txtBusinessCategory.leftView = categoryPadding ;
    UIView *cDropPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *cdropimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [cdropimg setImage:[UIImage imageNamed:@"dropDown.png"]];
    [cDropPadding addSubview:cdropimg];
    _txtBusinessCategory.rightViewMode = UITextFieldViewModeAlways;
    _txtBusinessCategory.rightView = cDropPadding;
    
    UIView *subCategoryPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *subCategoryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [subCategoryImage setImage:[UIImage imageNamed:@"subcategory.png"]];
    [subCategoryPadding addSubview:subCategoryImage];
    _txtSubCategory.leftViewMode = UITextFieldViewModeAlways;
    _txtSubCategory.leftView = subCategoryPadding ;
    UIView *scDropPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *scdropimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [scdropimg setImage:[UIImage imageNamed:@"dropDown.png"]];
    [scDropPadding addSubview:scdropimg];
    _txtSubCategory.rightViewMode = UITextFieldViewModeAlways;
    _txtSubCategory.rightView = scDropPadding;
   
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [dropDownView closeAnimation];
    
    if (textField == _txtcountry) {
        
        [self.countryView setHidden:NO];
        
        [_lblBusinessDetailsTitle setHidden:YES];
       
        return NO;
        
        
    }else if (textField == _txtDOB) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    else if (textField == _txtMobile) {
        
        
    }
    
    
    
    return true;
}

-(void)MobileDidChange :(UITextField *)theTextField{
  
    if (_txtMobile.text.length >0) {
         _txtMobile.placeholder = @"Mobile number";
        
    }else{
        _txtMobile.placeholder = @"+91 855xxxx629";
    }
    
    
}


-(void)businessDidChange :(UITextField *)theTextField
{
    
    if (_txtBusinessMobile.text.length >0) {
        _txtBusinessMobile.placeholder = @"Buisness mobile number";
        
    }else{
        _txtBusinessMobile.placeholder = @"+91 855xxxx629";
    }
    
    
}

//-(void)individualDidChange :(UITextField *)theTextField{
//
//    if (_txtIndividual.text.length >0) {
//        _txtIndividual.placeholder = @"       UserType";
//
//    }else{
//        _txtIndividual.placeholder = @"      Individual";
//    }
//
//
//}

-(void)countryDidChange :(UITextField *)theTextField{
    
    if (_txtcountry.text.length >0) {
        _txtcountry.placeholder = @"Country";
        
    }else{
        _txtcountry.placeholder = @"India";
    }
    
}

-(void)emailDidChange :(UITextField *)theTextField{
    
    if (_txtGmail.text.length >0) {
        _txtGmail.placeholder = @"Email";
        
    }else{
        _txtGmail.placeholder = @"abc@gmail.com (Optional)";
    }
    
}



-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    
   
        
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_txtfirstName) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    } else if (textField == _txtMiddleName) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    }else if (textField ==_txtLastName) {
        if (textField.text.length == 15 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    }else if (textField ==_txtMobile) {
        if (textField.text.length == 10 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    }
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}

- (IBAction)dateDoneClicked:(id)sender {
    [self ShowSelectedDate];
}

- (IBAction)backCountryClciked:(id)sender {
    
    [self.countryView setHidden:YES];
}



-(void)ShowSelectedDate{
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"dd-MM-yyyy"];
   NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    self.txtDOB.text = dateString ;
    NSDate *dateFromString = [[NSDate alloc] init];
     Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
   
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
     dtvalue = [dtTime integerValue];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = 270;
        [self.view layoutSubviews];
        
    }];
}


-(BOOL)emailStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [countryArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _countryTableView) {
        CountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"CountryCell" owner:self options:nil];

            cell= arr[0];

        }
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//        if (cell == nil) {
//            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//        }
        
        if([self.checkedIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.countryName.text = [[countryArray valueForKey:@"name"]objectAtIndex:indexPath.row];

        [cell.countryImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[countryArray  valueForKey:@"flagUrl"]objectAtIndex:indexPath.row]]] placeholderImage:nil];
        
       
       
        return cell;
        
        
    }
    return 0;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView
                                        cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    if([self.checkedIndexPath isEqual:indexPath])
    {
        self.checkedIndexPath = nil;
    }
    else
    {
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.checkedIndexPath = indexPath;
    }
    

    self.txtcountry.text = [[countryArray valueForKey:@"name"] objectAtIndex:indexPath.row];
    
    cuCode = [[countryArray valueForKey:@"code"] objectAtIndex:indexPath.row];
    
     [countryImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[countryArray valueForKey:@"flagUrl"]objectAtIndex:indexPath.row]]] placeholderImage:nil];
    
    [self.searchBar resignFirstResponder];
    [self.countryView setHidden:YES];
    [_lblBusinessDetailsTitle setHidden:NO];
    
    
}



-(void)CountryApiFromServer{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://192.168.1.95:8080/dndFilter/location/country"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
                [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        countryData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(countryMessage) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
                [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        
            }];
    
    
}
-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}


-(void)countryMessage {
    
    if ([[countryData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        countryArray = [countryData valueForKey:@"countryList"];
                        
        [self.countryTableView reloadData];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error From Server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }

    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.showsCancelButton = true;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
   self.checkedIndexPath = nil;
    
   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains %@", searchText];
   countryArray = [countryData valueForKey:@"countryList"];
    NSArray *filteredArray = [countryArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count != 0) {
        countryArray = [[NSMutableArray alloc]initWithArray:filteredArray];
    }
    
    
    [self.countryTableView reloadData];
}




-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
   
    [self.searchBar resignFirstResponder];
    self.searchBar.showsCancelButton = false;
    [self.countryView setHidden:YES];
 
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    
}




@end
