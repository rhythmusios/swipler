//
//  AccountVc.h
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountVc : UIViewController{
    id rechargeData;
    id walletData;
    NSString*consumerId;
    NSString*balnceAmount ;
    NSString*lastRecharge;
    NSString*lstWthdrw;
    id TransectionId;
//    NSString*consumerId;
}
@property (strong, nonatomic) IBOutlet UILabel *txtBalance;
@property (strong, nonatomic) IBOutlet UILabel *txtAccountBalance;
@property (strong, nonatomic) IBOutlet UILabel *txtLastSpent;
@property (strong, nonatomic) IBOutlet UIButton *btnRecharge;
@property (strong, nonatomic) IBOutlet UITableView *accountsTableView;
@property(strong,nonatomic)NSString* checkStr ;
@property (strong, nonatomic) IBOutlet UILabel *txtAccountType;

- (IBAction)rechargeClicked:(id)sender;
- (IBAction)backClicked:(id)sender;

@end
