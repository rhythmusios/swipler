//
//  ForgetPasswordVc.h
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPasswordVc : UIViewController

- (IBAction)backClicked:(id)sender;
@end
