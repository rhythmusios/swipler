//
//  ForgetPasswordVc.m
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "ForgetPasswordVc.h"

@interface ForgetPasswordVc ()

@end

@implementation ForgetPasswordVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
