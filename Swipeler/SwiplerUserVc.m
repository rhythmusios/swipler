//
//  SwiplerUserVc.m
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "SwiplerUserVc.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"
@interface SwiplerUserVc ()

@end

@implementation SwiplerUserVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIView *CardPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropcImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [dropcImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    [CardPadding addSubview:dropcImage];
    _txtnfcNumber.rightViewMode = UITextFieldViewModeAlways;
    _txtnfcNumber.rightView = CardPadding ;
    
    UIView *nfcnumberPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *NcImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [NcImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    [nfcnumberPadding addSubview:NcImage];
    _txtNFCCard.rightViewMode = UITextFieldViewModeAlways;
    _txtNFCCard.rightView = nfcnumberPadding ;
    
    cardListArray = [[NSMutableArray alloc]init];
    
    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [dropImage setImage:[UIImage imageNamed:@"dropDown.png"]];
    [dropDownPadding addSubview:dropImage];
    _txtName.rightViewMode = UITextFieldViewModeAlways;
    _txtName.rightView = dropDownPadding ;
    
    _scrollView.scrollEnabled = NO;
    
      [_authencationVw setHidden:YES];
    _viewAuthentication.constant = 0 ;
    [self.view layoutIfNeeded];
    
    [self.btnNfc setSelected:NO];
    [self.btnMobile setSelected:YES];
    
    relationArray = [[NSMutableArray alloc]initWithObjects:@"Self",@"Spouse",@"Parent",@"Friend",@"Relaive",@"Children",@"Other", nil];
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    
     self.btnSave.layer.cornerRadius = _btnSave.frame.size.height/2 ;
      self.btnCancel.layer.cornerRadius = _btnCancel.frame.size.height/2 ;
      self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;
    
    [self.txtNFCCard setHidden:YES];
    [self.lblNfc setHidden:YES];
    
    CGRect frm = self.btnSubmit.frame ;
    frm.origin.y = 128 ;
    self.btnSubmit.frame = frm ;
    
//    CGRect frm = self.btnSubmit.frame ;
//    frm.origin.y = 169 ;
//    self.btnSubmit.frame = frm ;
    
   
    
//    UIView *transPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
//    UILabel *trnsLbl = [[UILabel alloc] initWithFrame:CGRectMake(-30,-2, 50, 15)];
//    trnsLbl.text =  @"15,000";
//      trnsLbl.backgroundColor = [UIColor clearColor];
//    [transPadding addSubview:trnsLbl];
//     trnsLbl.font = [UIFont systemFontOfSize:15];
//    trnsLbl.textColor = [UIColor lightGrayColor];
//    _transectionLimit.rightViewMode = UITextFieldViewModeAlways;
//    _transectionLimit.rightView = transPadding ;
//
//
//    UIView *monhlyPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
//    UILabel *Monthlytlbl = [[UILabel alloc] initWithFrame:CGRectMake(-30,-2, 50, 15)];
//    Monthlytlbl.text =  @"30,000";
//    Monthlytlbl.backgroundColor = [UIColor clearColor];
//    Monthlytlbl.font = [UIFont systemFontOfSize:15];
//    Monthlytlbl.textColor = [UIColor lightGrayColor];
//    [monhlyPadding addSubview:Monthlytlbl];
//    _txtMonthlyLimit.rightViewMode = UITextFieldViewModeAlways;
//    _txtMonthlyLimit.rightView = monhlyPadding ;
//
    
    UIView *uploadPadding = [[UIView alloc] initWithFrame:CGRectMake(-20, -5, 25, 25)];

    switchButton= [[UISwitch alloc]init];
    switchButton.frame = CGRectMake(-20, -5, 25.0, 25.0);
    [switchButton setOn:YES animated:YES];
    [uploadPadding addSubview:switchButton];
    [switchButton addTarget:self
                     action:@selector(SwitchPressed:)
     forControlEvents:UIControlEventTouchUpInside];

switchButton.onTintColor = [UIColor colorWithRed:253/255.0f green:136/255.0f blue:136/255.0f alpha:1];
    [switchButton setOn:NO];
    
    
    
    _txtAuthentication.rightViewMode = UITextFieldViewModeAlways;
    _txtAuthentication.rightView = uploadPadding ;
    
    [self.view bringSubviewToFront:switchButton];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    selfCheck = false ;
    checkDropdown = false ;
    
    consumerId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    
    [_viewRelationship setHidden:YES];
    [_viewSelf setHidden:NO];
    
    _txtName.text = @"Self" ;
    
    
    if (_btnMobile.isSelected) {
        
        [self.txtNFCCard setHidden:YES];
        [self.lblNfc setHidden:YES];
    }else{
        [self.txtNFCCard setHidden:NO];
        [self.lblNfc setHidden:NO];
    }
    
    
    [self NfcCardsApiFromServe];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:dropDownView.view]) {
        
        
        
        return NO;
    }
    
    
    return YES;
}



-(IBAction)SwitchPressed:(id)sender{
    UISwitch *onoff = (UISwitch *) sender;
    NSLog(@"%@", onoff.on ? @"On" : @"Off");
    
    if(switchButton.isOn){
        _viewAuthentication.constant = 146 ;
        [self.view layoutIfNeeded];
        
          [_authencationVw setHidden:NO];
         _scrollView.scrollEnabled = YES;
        
        authRequest = @"true" ;
        
    }else{
        _viewAuthentication.constant = 0 ;
        [self.view layoutIfNeeded];
        [_authencationVw setHidden:YES];
         _scrollView.scrollEnabled = NO;
        [self.scrollView setContentOffset:CGPointZero animated:YES];
        
         authRequest = @"false" ;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}


- (IBAction)saveClicked:(id)sender {
    
    [KVNProgress show];
    [self CardActivateApiPostToServer];
    
//    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: nil
//                                                                              message: @"Otp sent to your registered mobile number"
//                                                                       preferredStyle:UIAlertControllerStyleAlert];
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Enter your otp";
//        textField.textColor = [UIColor blueColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//    }];
//
//    [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//        NSLog(@"Success");
//
//    }]];
//    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (IBAction)cancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)NfcClicked:(id)sender {
    [self.btnNfc setSelected:YES];
    [self.btnMobile setSelected:NO];
    
    
        [self.txtNFCCard setHidden:NO];
        [self.lblNfc setHidden:NO];
    CGRect frm = self.btnSubmit.frame ;
    frm.origin.y = 169 ;
    self.btnSubmit.frame = frm ;
    
}

- (IBAction)mobileClicked:(id)sender {
    [self.btnNfc setSelected:NO];
    [self.btnMobile setSelected:YES];
    
        [self.txtNFCCard setHidden:YES];
        [self.lblNfc setHidden:YES];
   
    CGRect frm = self.btnSubmit.frame ;
    frm.origin.y = 128 ;
    self.btnSubmit.frame = frm ;
    
}

- (IBAction)submitClicked:(id)sender {
    
    [KVNProgress show];
    [self CardActivateApiPostToServer];
    
//    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: nil
//                                                                              message: @"Otp sent to your registered mobile number"
//                                                                       preferredStyle:UIAlertControllerStyleAlert];
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//        textField.placeholder = @"Enter your otp number";
//        textField.textColor = [UIColor blueColor];
//        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        textField.borderStyle = UITextBorderStyleRoundedRect;
//    }];
//
//    [alertController addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//        NSLog(@"Success");
//
//    }]];
//    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (IBAction)doneClicked:(id)sender {
    [self ShowSelectedDate];
}

-(void)ShowSelectedDate{
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"dd-MM-yyyy"];
    NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    self.txtDob.text = dateString ;
    NSDate *dateFromString = [[NSDate alloc] init];
    Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
    
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    dtvalue = [dtTime integerValue];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomPicker.constant = 270;
        [self.view layoutSubviews];
        
    }];
}



-(void)tapGesture{
    [self.view endEditing:YES];
    [dropDownView closeAnimation];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    selfCheck = false ;
    [dropDownView.view removeFromSuperview];
    
    [dropDownView closeAnimation];
    
    [self.view endEditing:YES];
    
    if (textField == _txtName) {
        
        
        
        dropDownView = [[DropDownView alloc] initWithArrayData:relationArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
        
        
        
        dropDownView.delegate = self;
        dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
        
        
        [self.scrollView addSubview:dropDownView.view];
        
        [self.view bringSubviewToFront:dropDownView.view];
        
        _dropDownTxtfield = _txtName ;
        
        [ dropDownView openAnimation];
        
        return NO ;
        
        
    }else if (textField == _txtnfcNumber) {
        checkDropdown = true;
        
        
       
        
        if (cardListArray.count >0) {
            dropDownView = [[DropDownView alloc] initWithArrayData:cardListArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
            
            
            
            dropDownView.delegate = self;
            dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
            
            [self.scrollView addSubview:dropDownView.view];
            
            [self.view bringSubviewToFront:dropDownView.view];
            
            _dropDownTxtfield = _txtnfcNumber ;
            
            [ dropDownView openAnimation];
            
        }else{
        [self  NfcCardsApiFromServe];
        }
        
        return NO ;
        
        
    }else if (textField == _txtNFCCard) {
        selfCheck = true;
        checkDropdown = true ;
       
        
        if (cardListArray.count >0) {
            dropDownView = [[DropDownView alloc] initWithArrayData:cardListArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:textField animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
            
            
            
            dropDownView.delegate = self;
            dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
            
            [self.viewSelf addSubview:dropDownView.view];
            
            [self.view bringSubviewToFront:dropDownView.view];
            
            _dropDownTxtfield = _txtNFCCard ;
            
            [ dropDownView openAnimation];
            
        }else{
            [self  NfcCardsApiFromServe];
        }
        
        return NO ;
        
        
    }
    
    else if (textField == _txtDob) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomPicker.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    else
    {
        return YES;
    }
    
}


-(void)dropDownCellSelected:(NSInteger)returnIndex{
    
    if (_dropDownTxtfield == _txtName) {
        
        _txtName.text = [relationArray objectAtIndex:returnIndex];
        if ([_txtName.text isEqualToString:@"Self"]) {
            
            [_viewRelationship setHidden:YES];
            [_viewSelf setHidden:NO];
            
        }else{
            
            [_viewRelationship setHidden:NO];
            [_viewSelf setHidden:YES];
        }
        
    }else if (_dropDownTxtfield == _txtnfcNumber) {
        
        _txtnfcNumber.text = [cardListArray objectAtIndex:returnIndex];
        
    }
    else if (_dropDownTxtfield == _txtNFCCard) {
        
        _txtNFCCard.text = [cardListArray objectAtIndex:returnIndex];
        
    }
    
}


-(void)NfcCardsApiFromServe{
    
    [dropDownView.view removeFromSuperview];
    [dropDownView closeAnimation];
    
    [self.view endEditing:YES];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/crdInv/%@",consumerId];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        cardData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(ResponseMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}

-(void)ResponseMessage
{
    
    if ([[cardData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
      
        cardListArray = [[cardData valueForKey:@"cardLst"]valueForKey:@"cardId"];
        
        NSLog(@"%@",cardListArray);
        
        if (cardListArray.count >0) {
            dropDownView = [[DropDownView alloc] initWithArrayData:cardListArray cellHeight:40 heightTableView:120 paddingTop:0  paddingLeft:0 paddingRight:0 refView:_txtnfcNumber animation:BLENDIN openAnimationDuration:1 closeAnimationDuration:0.5];
            
            
            
            dropDownView.delegate = self;
            dropDownView.view.backgroundColor = [UIColor colorWithRed:245/255.0f green:115/255.0 blue:115/255.0 alpha:1];
            if (selfCheck == true) {
                [self.viewSelf addSubview:dropDownView.view];
                
            }else{
                [self.scrollView addSubview:dropDownView.view];
            }
            
            [self.view bringSubviewToFront:dropDownView.view];
            
            _dropDownTxtfield = _txtnfcNumber ;
            if (checkDropdown == true) {
                [ dropDownView openAnimation];
            }
        }else{
            
            UIAlertView*alertNoCard = [[UIAlertView alloc]initWithTitle:nil message:@"No Nfc Cards are issued" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            alertNoCard.tag = 11 ;
            alertNoCard.delegate = self;
            [alertNoCard show];
            
        }
        
        
        
    }
    else
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[cardData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)kvnDismiss{
    
    [KVNProgress dismiss];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag ==11){
        
       
            
            [self.navigationController popViewControllerAnimated:YES];
            
        
    }
    
}

-(void)CardActivateApiPostToServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/card/activate"];
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    
    NSInteger consumr = [consumerId integerValue];
    
    double authenLimit = [_txtAuthenlimit.text doubleValue];
     double authLimit = [_txtAuthLimit.text doubleValue];
     double transLmt = [_txtTransection.text doubleValue];
    
    
    NSDictionary* params = [[NSDictionary alloc] init];
    params = @{
               @"cardId" : @"142343545364",
               @"consumerId" :[NSString stringWithFormat:@"%ld",consumr],
               @"firstName" :_txtFirstName.text,
               @"lastName": _txtLastName.text,
               @"mobile": _txtMobile.text,
               @"dob": [NSString stringWithFormat:@"%ld",dtvalue],
               @"adharCard": _txtAadhar.text,
               @"authReq": authRequest,
               @"transLmt":[NSString stringWithFormat:@"%f",transLmt],
               @"authLmt":[NSString stringWithFormat:@"%f",authLimit],
               @"mnthlyLmt": [NSString stringWithFormat:@"%f",authenLimit]

               };
    
//    NSDictionary *parameters = @{
//                                 @"adharCard": @"788797979779",
//                                  @"authLmt": @"20000.000",
//                                  @"authReq": @"true",
//                                  @"cardId": @"BSBBS5252FDF",
//                                  @"consumerId": @"1466",
//                                  @"dob": @"1518739200000",
//                                  @"firstName": @"luckei",
//                                  @"lastName": @"Katoch",
//                                  @"mnthlyLmt": @"50000.000",
//                                  @"mobile": @"8556841629",
//                                  @"transLmt": @"10000.000"
//
//                                  };
//
    
    [managerr POST:apiURLStr parameters:params success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        cardActivateData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(CardActivateResponse) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error from server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }];
    
    
}
-(void)CardActivateResponse{
    
    if ([[cardActivateData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[cardActivateData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_txtAadhar) {
        if (textField.text.length == 12 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
    }
   
    
    return YES;
    
}


@end
