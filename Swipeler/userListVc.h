//
//  userListVc.h
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface userListVc : UIViewController{
    
    NSString* consumerId ;
    id crdHdrData;
    
    NSMutableArray*CrdHdrArray;
}
@property (strong, nonatomic) IBOutlet UITableView *usersTableView;
- (IBAction)addClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblNoUser;
@property (strong, nonatomic) IBOutlet UILabel *viewHeader;

@end
