//
//  PPTransferVc.h
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPTransferVc : UIViewController<UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtName;

@property (strong, nonatomic) IBOutlet UITextField *txtNFC;

@property (strong, nonatomic) IBOutlet UITextField *txtAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnProceed;

@property (strong, nonatomic) IBOutlet UIView *viewSubAlert;


@property (strong, nonatomic) IBOutlet UIView *viewAlertPayment;
@property (strong, nonatomic) IBOutlet UIButton *btnPayment;
@property (strong, nonatomic) IBOutlet UILabel *txtUserName;
@property (strong, nonatomic) IBOutlet UILabel *txtId;

@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

- (IBAction)cancelClicked:(id)sender;
- (IBAction)proceedPaymentClicked:(id)sender;
- (IBAction)txtProceed:(id)sender;
- (IBAction)backClicked:(id)sender;

@end
