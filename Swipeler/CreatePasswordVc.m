//
//  CreatePasswordVc.m
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "CreatePasswordVc.h"
#import "HomeVc.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"

@interface CreatePasswordVc ()

@end

@implementation CreatePasswordVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    
    self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;

    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    UIView *passwordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *passwordImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [passwordImage setImage:[UIImage imageNamed:@"pass.png"]];
    [passwordPadding addSubview:passwordImage];
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtPassword.leftView = passwordPadding ;
    
    
    UIView *ConpasswordPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *conpasswordImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [conpasswordImage setImage:[UIImage imageNamed:@"pass.png"]];
    [ConpasswordPadding addSubview:conpasswordImage];
    _txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;
    _txtConfirmPassword.leftView = ConpasswordPadding ;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapGesture{
    [self.view endEditing:YES];
}

- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitClicked:(id)sender {
    
    
    NSString*aString;
    aString = _txtPassword.text ;
    
    NSCharacterSet *CaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
   
    
    
    if ([self.txtPassword.text isEqualToString:@""]||self.txtPassword.text.length < 8) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Enter at least 8 characters of  password"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
   else  if ([aString rangeOfCharacterFromSet:CaseChars].location == NSNotFound || [aString rangeOfCharacterFromSet:numbers].location == NSNotFound) {
               UIAlertController * alert = [UIAlertController
                                            alertControllerWithTitle:nil
                                            message:@"Password should contain atleast one character and numeric value"
                                            preferredStyle:UIAlertControllerStyleAlert];
               UIAlertAction* noButton = [UIAlertAction
                                          actionWithTitle:@"Ok"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
       
                                          }];
       
               [alert addAction:noButton];
               [self presentViewController:alert animated:YES completion:nil];
    }
//   else if ([_txtPassword.text rangeOfCharacterFromSet:numericSet].location != NSNotFound) {
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:nil
//                                     message:@"Enter atleast one numeric value"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* noButton = [UIAlertAction
//                                   actionWithTitle:@"Ok"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//                                   }];
//
//        [alert addAction:noButton];
//        [self presentViewController:alert animated:YES completion:nil];
//    }

//    else if (![self isValidPassword:self.txtPassword.text]){
//
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:nil
//                                     message:@"Enter at least 8 characters of alphanumeric password"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* noButton = [UIAlertAction
//                                   actionWithTitle:@"Ok"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//                                   }];
//
//        [alert addAction:noButton];
//        [self presentViewController:alert animated:YES completion:nil];
//
//
//    }
    
    else if (![self.txtConfirmPassword.text isEqualToString:_txtPassword.text]){
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Passwords are not matching"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [self kvnStart];
        [self sendPasswordFromApi];
    }
}

-(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}


-(void)sendPasswordFromApi{
    
    NSString* strPassword= self.txtPassword.text;
    NSData *dataPass = [strPassword dataUsingEncoding:NSUTF8StringEncoding];
    
    // Convert to Base64 data
    NSData *base64Data = [dataPass base64EncodedDataWithOptions:0];
   NSString* Base64encodePassword = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:[base64Data bytes]]];
    
    NSLog(@"%@", Base64encodePassword);
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/setpass"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString*token = [[NSUserDefaults standardUserDefaults]valueForKey:@"tokenID"];
    
    
    [managerr.requestSerializer setValue:token forHTTPHeaderField:@"token"];

    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"cnsmrId" :_ConsumerID,
                @"pswrd" :Base64encodePassword
               
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
                     passwordData = responseObject;
         [self performSelectorOnMainThread:@selector(checkResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
    
}
-(void)checkResponse{
    
    if ([[passwordData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        NSString*consumerId = [passwordData valueForKey:@"cnsmrId"];
        [[NSUserDefaults standardUserDefaults]setValue:consumerId forKey:@"consumerId"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        HomeVc *home = [[HomeVc alloc]init];
        home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVc"];
       
    
        
        [self.navigationController pushViewController:home animated:YES];

    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Error From Server"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)kvnDismiss{

    [KVNProgress dismiss];
}

-(void)kvnStart{
    
    [KVNProgress show];
}

- (BOOL)isAlphanumeric:(NSString *)string {
    
    return [string rangeOfString:@"^[-0-9]" options:NSRegularExpressionSearch].location != NSNotFound;
    
}


               
@end
