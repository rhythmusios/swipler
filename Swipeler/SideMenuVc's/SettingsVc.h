//
//  SettingsVc.h
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVc : UIViewController


- (IBAction)backClicked:(id)sender;

@end
