//
//  UpdateVc.m
//  Swipeler
//
//  Created by katoch on 19/01/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import "UpdateVc.h"

@interface UpdateVc ()

@end

@implementation UpdateVc

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self textfieldsPadding];
    
    _txtDOB.text = _DOBstr ;
    _txtGmail.text = _Gmailstr ;
    _txtMobile.text = _Mobilestr ;
    _txtLastName.text = _LastName ;
    _txtMiddleName.text = _MiddleName ;
    _txtfirstName.text = _firstName ;
    _txtcountry.text = _country ;
    _txtAdharNumber.text = _AdharNumber ;
    _txtAdharMobile.text = _adharMobile ;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

-(void)textfieldsPadding{
    
    UIImageView*countryImage;
    UIView *userTypePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    countryImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [countryImage setImage:[UIImage imageNamed:@"india.png"]];
    [userTypePadding addSubview:countryImage];
    _txtcountry.leftViewMode = UITextFieldViewModeAlways;
     _txtcountry.leftView = userTypePadding ;
    UIView *userPadding;
    UIImageView *image;
    if ([UIScreen mainScreen].bounds.size.width==320) {
        userPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _txtLastName.font = [UIFont systemFontOfSize:12];
        _txtfirstName.font = [UIFont systemFontOfSize:12];
        _txtMiddleName.font = [UIFont systemFontOfSize:12];
        
    }else{
        userPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        _txtLastName.font = [UIFont systemFontOfSize:14];
        _txtfirstName.font = [UIFont systemFontOfSize:14];
        _txtMiddleName.font = [UIFont systemFontOfSize:14];
        
    }
    [image setImage:[UIImage imageNamed:@"name.png"]];
    [userPadding addSubview:image];
    _txtfirstName.leftViewMode = UITextFieldViewModeAlways;
    _txtfirstName.leftView = userPadding ;

//    UIView *dropDownPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
//    UIImageView *dropImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
//    [dropImage setImage:[UIImage imageNamed:@"dropDown.png"]];
//    [dropDownPadding addSubview:dropImage];
//    _txtcountry.rightViewMode = UITextFieldViewModeAlways;
//    _txtcountry.rightView = dropDownPadding ;
//
    
   
    
    UIView *mobilePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *mobileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [mobileImage setImage:[UIImage imageNamed:@"mobile.png"]];
    [mobilePadding addSubview:mobileImage];
    _txtMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtMobile.leftView = mobilePadding ;
    
    UIView *emailPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *emailImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [emailImage setImage:[UIImage imageNamed:@"email.png"]];
    [emailPadding addSubview:emailImage];
    _txtGmail.leftViewMode = UITextFieldViewModeAlways;
    _txtGmail.leftView = emailPadding ;
    
    UIView *dobPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    UIImageView *dobimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [dobimage setImage:[UIImage imageNamed:@"calender.png"]];
    [dobPadding addSubview:dobimage];
    _txtDOB.leftViewMode = UITextFieldViewModeAlways;
    _txtDOB.leftView = dobPadding ;
    
  
   
    
  
    
    
    UIView *aadharPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *aadharimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [aadharimage setImage:[UIImage imageNamed:@"aadhar-Small@2x.png"]];
    [aadharPadding addSubview:aadharimage];
    _txtAdharNumber.leftViewMode = UITextFieldViewModeAlways;
    _txtAdharNumber.leftView = aadharPadding ;
    
    UIView *uploadPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *uploadimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [uploadimage setImage:[UIImage imageNamed:@"mobile.png"]];
    [uploadPadding addSubview:uploadimage];
    _txtAdharMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtAdharMobile.leftView = uploadPadding ;
    self.btnUpdate.layer.cornerRadius = _btnUpdate.frame.size.height/2 ;
    
    
}



- (IBAction)updateClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}


-(void)ShowSelectedDate{
    
    NSDateFormatter *Df = [[NSDateFormatter alloc]init];
    [Df setDateFormat:@"dd-MM-yyyy"];
    NSString*dateString =[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicker.date]];
    self.txtDOB.text = dateString ;
    NSDate *dateFromString = [[NSDate alloc] init];
    Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFromString = [Df dateFromString:dateString];
    
    NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
    
    
    NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
    dtvalue = [dtTime integerValue];
    [UIView animateWithDuration:0.5 animations:^{
        self.bottomDate.constant = 270;
        [self.view layoutSubviews];
        
    }];
}

- (IBAction)dateDoneClicked:(id)sender {
    [self ShowSelectedDate];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
   
    
    if (textField == _txtcountry) {
        
    
        return NO;
        
        
    }
    else if (textField == _txtfirstName || textField == _txtMiddleName || textField == _txtLastName) {
        
        return YES;
    }
    
    else if (textField == _txtDOB) {
        
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    
    else if (textField == _txtMobile) {
        
        return NO;
    }
    else if (textField == _txtGmail) {
        
        return YES;
    }
    else if (textField == _txtAdharNumber) {
        
        return YES;
    }else if (textField == _txtAdharMobile) {
        
        return YES;
    }

    
    
    
    return true;
}

@end
