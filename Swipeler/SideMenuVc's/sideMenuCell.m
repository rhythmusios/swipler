//
//  sideMenuCell.m
//  CityBus
//
//  Created by katoch on 29/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "sideMenuCell.h"

@implementation sideMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
