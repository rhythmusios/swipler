//
//  ProfileVc.m
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "ProfileVc.h"
#import "sideMenuCell.h"
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "UpdateVc.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"

@interface ProfileVc ()
@end

@implementation ProfileVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_btnEdit setBackgroundColor:[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.3]];
    
    
      [self setAlertCtrl];
    _profileImage.layer.cornerRadius = self.profileImage.frame.size.width/2 ;
    
    _viewImage.layer.cornerRadius = self.viewImage.frame.size.width/2 ;
    
    _viewImage.layer.masksToBounds = YES;

    
   
    
    
    
    
  
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self profileApiFromServer];
    
}
- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return listArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return 55 ;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        sideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"sideMenuCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
    
            cell.txtMenu.text  = [NSString stringWithFormat:@"%@",[listArray objectAtIndex:indexPath.row]];
            cell.menuImg.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    
        
        return cell;
    
}

///upload

-(void)setAlertCtrl;
{
    self.AlertCtrl = [UIAlertController alertControllerWithTitle:@"selectImage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             
                             {
                                 [self camera];
                                 
                                 isCamera = true;
                                 
                                 
                             }];
    
    UIAlertAction *Library  = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action)
                               
                               {
                                   
                                   [self selectPhoto];
                                   
                                   isCamera = false;
                               }];
    
    UIAlertAction *Cancel  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        isCamera = false;
        
    }];
    
    [self.AlertCtrl addAction:camera];
    [self.AlertCtrl addAction:Library];
    [self.AlertCtrl addAction:Cancel];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

-(void)camera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        
    } else {
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    
    if (isCamera == true) {
        
        checqImage = info[UIImagePickerControllerEditedImage];
       
        
         _profileImage.image = checqImage ;
        
        _backgroundImg.image = checqImage ;
//        NSData *webData = UIImagePNGRepresentation(checqImage);
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//
//        self.txtCancelledCheck.text = documentsDirectory ;
        UIImageWriteToSavedPhotosAlbum(checqImage, nil, nil, nil);
        
        
        
    }else
    {
        
        
        checqImage = info[UIImagePickerControllerEditedImage];
        
        _profileImage.image = checqImage ;
         _backgroundImg.image = checqImage ;
        NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
//        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
//        NSString *filename = [[result firstObject] filename];
//        self.txtCancelledCheck.text = filename ;
//
        
    }
    
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)editImageClicked:(id)sender {
    
    [self requestAuthorizationWithRedirectionToSettings];
    
}

- (IBAction)editClicked:(id)sender {
    
    UpdateVc *Cvc = [[UpdateVc alloc]init];
    Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateVc"];
    Cvc.DOBstr = [NSString stringWithFormat:@"%@",dob];
    Cvc.Gmailstr  =  email ;
    Cvc.Mobilestr = [NSString stringWithFormat:@"%@",Mobile];
    Cvc.country = [NSString stringWithFormat:@"%@",[profileData valueForKey:@"country"]];
    Cvc.firstName = [profileData valueForKey:@"firstName"];
    Cvc.MiddleName = [profileData valueForKey:@"middleName"];
    Cvc.LastName = [profileData valueForKey:@"lastName"];
     Cvc.AdharNumber = [profileData valueForKey:@"aadharNumber"];
    
    NSString*addarNumber = [profileData valueForKey:@"aadharRegistereMobileNo"];
    
    if ([addarNumber isEqual:[NSNull null]]) {
        
    }else{
    Cvc.adharMobile = addarNumber ;
    }
   
    
  [self.navigationController pushViewController:Cvc animated:YES];
    
}




- (void)requestAuthorizationWithRedirectionToSettings {
    dispatch_async(dispatch_get_main_queue(), ^{
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusAuthorized)
        {
            isCamera = false ;
            
            [self presentViewController:self.AlertCtrl animated:YES completion:nil];

        }
        else
        {
            //No permission. Trying to normally request it
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                if (status != PHAuthorizationStatusAuthorized)
                {
                    //User don't give us permission. Showing alert with redirection to settings
                    //Getting description string from info.plist file
                    NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:cancelAction];
                    
                    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                    }];
                    [alertController addAction:settingsAction];
                    
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                }
            }];
        }
    });
}



-(void)profileApiFromServer{
    
   NSString* consumerId =   [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/usr/consumerprofile?consumerId=%@",consumerId];
    
    
    
    
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
       profileData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(profileMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}
-(void)profileMessage
{
    
//    if ([[profileData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
    
        CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
        
        //Setting label according to labelText
        NSString*str = [NSString stringWithFormat:@"%@ %@",[profileData valueForKey:@"firstName"] ,[profileData valueForKey:@"lastName"]];
        
        CGSize expectedLabelSize = [str sizeWithFont:_lblName.font constrainedToSize:maximumLabelSize lineBreakMode:_lblName.lineBreakMode];
        CGRect newFrame = _lblName.frame;
        newFrame.size.width = expectedLabelSize.width;
        _lblName.frame = newFrame;
        _lblName.text = str ;
    
    NSString*strDob = [profileData valueForKey:@"dob"];
    double valueDob = [strDob doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(valueDob / 1000.0)];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd-MM-yyyy"]; // Date formater
    NSString *dateStr = [dateformate stringFromDate:date]; // Convert date to string
   
    dob = dateStr ;
    
    email = [profileData valueForKey:@"email"];
    Mobile = [profileData valueForKey:@"mobileNo"];
    gender =   [profileData valueForKey:@"userType"];
    
    aadhaar = [profileData valueForKey:@"aadharNumber"];
    
         listArray = [[NSMutableArray alloc]initWithObjects:email,Mobile,gender,dob,aadhaar, nil];
        
        imagesArray = [[NSMutableArray alloc]initWithObjects:@"email.png",@"mobile.png",@"name.png",@"calender.png",@"aadhar-Small@2x.png", nil];
    
    
       [_profileTableView reloadData];
        
//    }else{
//
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:nil
//                                     message:[profileData valueForKey:@"message"]
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction* noButton = [UIAlertAction
//                                   actionWithTitle:@"Ok"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction * action) {
//
//
//                                   }];
//
//        [alert addAction:noButton];
//        [self presentViewController:alert animated:YES completion:nil];
//
//    }
    
}

-(void)kvnDismiss{
    
    [KVNProgress dismiss];
    
}

@end
