//
//  OffersVc.m
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "OffersVc.h"

@interface OffersVc ()

@end

@implementation OffersVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
