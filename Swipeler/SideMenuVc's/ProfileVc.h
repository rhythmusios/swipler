//
//  ProfileVc.h
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVc : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>{
    
    NSMutableArray*listArray;
    NSMutableArray*imagesArray;
    UIImage*checqImage;
    BOOL isCamera ;
    id profileData;
    
    NSString*email;
    NSString*Mobile;
    NSString*gender;
    NSString*dob;
    
    NSString*aadhaar;
    
}
@property (strong, nonatomic) IBOutlet UIView *viewImage;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImg;

@property (strong, nonatomic) IBOutlet UIAlertController *AlertCtrl;
- (IBAction)backClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *profileTableView;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *editProfile;
@property (strong, nonatomic) IBOutlet UILabel *txtName;


- (IBAction)editImageClicked:(id)sender;
- (IBAction)editClicked:(id)sender;


@end
