//
//  UpdateVc.h
//  Swipeler
//
//  Created by katoch on 19/01/18.
//  Copyright © 2018 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateVc : UIViewController{
     NSInteger dtvalue ;
}

@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UITextField *txtAdharMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtAdharNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtfirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtDOB;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtGmail;
@property (strong, nonatomic) IBOutlet UITextField *txtcountry;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;


@property(strong,nonatomic) NSString* adharMobile;
@property(strong,nonatomic) NSString* AdharNumber;
@property(strong,nonatomic) NSString* firstName;
@property(strong,nonatomic) NSString* MiddleName;
@property(strong,nonatomic) NSString* LastName;
@property(strong,nonatomic) NSString* DOBstr;
@property(strong,nonatomic) NSString* Mobilestr;
@property(strong,nonatomic) NSString* Gmailstr;
@property(strong,nonatomic) NSString* country;


- (IBAction)updateClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)dateDoneClicked:(id)sender;



@end
