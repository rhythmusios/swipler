//
//  ChangePasswordVc.m
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "ChangePasswordVc.h"

@interface ChangePasswordVc ()

@end

@implementation ChangePasswordVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
