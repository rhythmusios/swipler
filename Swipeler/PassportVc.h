//
//  PassportVc.h
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import <Photos/Photos.h>
#import <AFNetworking/AFNetworking.h>

@interface PassportVc : UIViewController<UITextFieldDelegate,DropDownViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>{
    
   BOOL isCamera ;
   
    
     UIImage *frontImage ;
    UIImage*BackImage;
    NSString*checkFrontBack;
    NSString*checkExpiry;
    
    UIImage*aadharImage;
    id  signupData ;
    NSInteger issueDate ;
    NSInteger expiryDate;
    NSString*  consumerId ;
    NSString*registeredOtp;
    id verifiedData;
    id resendData;
    
}



@property (strong, nonatomic) IBOutlet UIView *viewOtp;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicked;
@property (strong, nonatomic) IBOutlet UITextField *txtPassport;
@property (strong, nonatomic) IBOutlet UITextField *txtfront;
@property (strong, nonatomic) IBOutlet UITextField *txtLast;
@property (strong, nonatomic) IBOutlet UITextField *txtLocalMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtExpiry;
@property (strong, nonatomic) IBOutlet UITextField *txtIssue;
@property (strong, nonatomic) IBOutlet UITextField *txtOtp;
@property (strong, nonatomic) IBOutlet UIButton *btnVerify;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIAlertController *AlertCtrl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;

@property (strong, nonatomic)NSString*countryName ;
@property (strong, nonatomic)NSString*countryCode ;
@property (strong, nonatomic)NSString*firstName ;
@property (strong, nonatomic)NSString*lastName ;
@property (strong, nonatomic)NSString*middleName ;
@property (strong, nonatomic)NSString*userType ;
@property (strong, nonatomic)NSString*DOB;
@property (strong, nonatomic)NSString*mobile;
@property (strong, nonatomic)NSString*gmail;

- (IBAction)submitClicked:(id)sender;
- (IBAction)resendClicked:(id)sender;
- (IBAction)verifyClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)doneClicked:(id)sender;

@end
