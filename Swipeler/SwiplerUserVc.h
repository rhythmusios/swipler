//
//  SwiplerUserVc.h
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface SwiplerUserVc : UIViewController<UIGestureRecognizerDelegate,DropDownViewDelegate,UIGestureRecognizerDelegate>{
    UISwitch *switchButton;
    
    NSMutableArray*relationArray;
     DropDownView *dropDownView ;
      NSInteger dtvalue ;
    id cardData;
    NSMutableArray *cardListArray ;
    
    id cardActivateData;
    NSString* consumerId;
    NSString*authRequest ;
    
    BOOL checkDropdown;
    BOOL  selfCheck ;
    
}
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
//@property (strong, nonatomic) IBOutlet UITextField *txtNfc;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtAuthentication;
@property (strong, nonatomic) IBOutlet UITextField *txtDob;
@property (strong, nonatomic) IBOutlet UITextField *txtTransection;
@property (strong, nonatomic) IBOutlet UITextField *txtAuthLimit;
@property (strong, nonatomic) IBOutlet UITextField *txtAadhar;

@property (strong, nonatomic) IBOutlet UIView *viewSelf;
@property (strong, nonatomic) IBOutlet UIView *viewRelationship;
@property (strong, nonatomic) UITextField *dropDownTxtfield;
@property (strong, nonatomic) IBOutlet UITextField *txtNFCCard;
@property (strong, nonatomic) IBOutlet UILabel *lblNfc;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomPicker;
@property (strong, nonatomic) IBOutlet UITextField *txtnfcNumber;


//@property (strong, nonatomic) IBOutlet UITextField *txtMonthlyLimit;
//@property (strong, nonatomic) IBOutlet UITextField *transectionLimit;


@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnNfc;
@property (strong, nonatomic) IBOutlet UIButton *btnMobile;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewAuthentication;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *authencationVw;
@property (strong, nonatomic) IBOutlet UITextField *txtAuthenlimit;

- (IBAction)saveClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)NfcClicked:(id)sender;
- (IBAction)mobileClicked:(id)sender;
- (IBAction)submitClicked:(id)sender;
- (IBAction)doneClicked:(id)sender;





@end
