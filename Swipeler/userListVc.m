//
//  userListVc.m
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "userListVc.h"
#import "UserListCell.h"
#import "SwiplerUserVc.h"
#import <AFNetworking/AFNetworking.h>
#import "KVNProgress.h"



@interface userListVc ()
{
    NSMutableArray * MobileArray;
    NSMutableArray * NameArray;
}
@end

@implementation userListVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    CrdHdrArray = [[NSMutableArray alloc]init];
    
    
    _usersTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    MobileArray = [[NSMutableArray alloc]initWithObjects:@"6356256565",@"7890987645",@"6778987886",@"7668952468", nil];
    NameArray = [[NSMutableArray alloc]initWithObjects:@"Smith",@"Jhon",@"Richa",@"Rock", nil];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    consumerId = [[NSUserDefaults standardUserDefaults]valueForKey:@"consumerId"];
    [KVNProgress show];
    
    [self cardHoldrApiFromServer];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return CrdHdrArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _usersTableView) {
        UserListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserListCell"];
        if (cell==nil) {
            NSArray *arr = [[NSBundle mainBundle]loadNibNamed:@"UserListCell" owner:self options:nil];
            
            cell= arr[0];
            
        }
        
        NSString *firstName = [NSString stringWithFormat:@"%@",[[CrdHdrArray valueForKey:@"firstName"]objectAtIndex:indexPath.row]];
        
         NSString *lastName = [NSString stringWithFormat:@"%@",[[CrdHdrArray valueForKey:@"lastName"]objectAtIndex:indexPath.row]];
        
        cell.txtName.text =[NSString stringWithFormat:@"%@ %@",firstName ,lastName];
        cell.txtSN.text =[[CrdHdrArray valueForKey:@"mobile"] objectAtIndex:indexPath.row];
        
        
        return cell;
        
        
    }
    return 0;
    
}


- (IBAction)addClicked:(id)sender {
    SwiplerUserVc *SUVc = [[SwiplerUserVc alloc]init];
    SUVc = [self.storyboard instantiateViewControllerWithIdentifier:@"SwiplerUserVc"];
    [self.navigationController pushViewController:SUVc animated:YES];
}

- (IBAction)backClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)cardHoldrApiFromServer{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/secure/cnsmr/crdhldr/%@",consumerId];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [managerr GET:apiURLStr parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
        
        crdHdrData = responseObject ;
        
        [self performSelectorOnMainThread:@selector(ResponseMessage) withObject:nil waitUntilDone:YES];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error) {
              [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
              NSLog(@"Error: %@", error);
              
          }];
    
}

-(void)ResponseMessage
{
    if ([[crdHdrData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]) {
        
        CrdHdrArray = [crdHdrData valueForKey:@"cardLst"];
        
        if ([CrdHdrArray count] > 0) {
            
            [self.lblNoUser setHidden:YES];
             [self.usersTableView setHidden:NO];
            [self.viewHeader setHidden:NO];
            [self.usersTableView reloadData];
            
            
        }else{
            [self.lblNoUser setHidden:NO];
             [self.viewHeader setHidden:YES];
            [self.usersTableView setHidden:YES];
        }
      
    }
    else
    {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[crdHdrData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)kvnDismiss{
    
    [KVNProgress dismiss];
    
}
@end
