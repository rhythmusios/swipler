//
//  OtpVc.m
//  CityBus
//
//  Created by katoch on 19/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "OtpVc.h"
#import "HomeVc.h"
#import "CreatePasswordVc.h"
#import <AFNetworking/AFNetworking.h>

@interface OtpVc ()

@end

@implementation OtpVc

- (void)viewDidLoad {
    [super viewDidLoad];
   self.btnVarify.layer.cornerRadius = _btnVarify.frame.size.height/2 ;
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
}

-(void)tapGesture{
    
    [_txtOtp resignFirstResponder];
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)VarifyClicked:(id)sender {
    CreatePasswordVc * viewStep = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
    
    viewStep.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController pushViewController:viewStep animated:YES];
   
}

- (IBAction)resendOtpClicked:(id)sender {
    
    
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



@end
