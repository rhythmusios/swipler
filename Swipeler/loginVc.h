//
//  loginVc.h
//  CityBus
//
//  Created by katoch on 18/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface loginVc : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,DropDownViewDelegate>{
    
    id loginData;
     DropDownView *dropDownView ;
    NSString*userType;
    NSMutableArray*dropDownArray;
}


@property (strong, nonatomic) IBOutlet UIView *viewLogin;
@property (strong, nonatomic) IBOutlet UITextField *userType;
@property (strong, nonatomic) UITextField *dropDownTxtfield;


@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnForget;
@property (strong, nonatomic) IBOutlet UIImageView *firstImage;
@property (strong, nonatomic) IBOutlet UIImageView *secondImage;



- (IBAction)loginClicked:(id)sender;
- (IBAction)registerClicked:(id)sender;
- (IBAction)forgetClicked:(id)sender;



@end
