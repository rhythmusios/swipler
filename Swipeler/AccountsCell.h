//
//  AccountsCell.h
//  CityBus
//
//  Created by katoch on 25/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *txtAmount;
@property (strong, nonatomic) IBOutlet UILabel *txtTransId;
@property (strong, nonatomic) IBOutlet UILabel *txtName;
@property (strong, nonatomic) IBOutlet UILabel *txtDate;

@end
