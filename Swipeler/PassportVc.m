//
//  PassportVc.m
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "PassportVc.h"
#import <QuartzCore/QuartzCore.h>
#import "CreatePasswordVc.h"
#import "KVNProgress.h"

@interface PassportVc ()

@end

@implementation PassportVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [_txtLocalMobile addTarget:self action:@selector(localMobileDidChange:) forControlEvents:UIControlEventEditingChanged];
    _viewOtp.hidden = YES;
    _btnVerify.layer.borderWidth = 1.0f;
    _btnVerify.layer.borderColor = [UIColor colorWithRed:245/255.0f green:115/255.0f blue:115/255.0f alpha:1].CGColor;
    
    
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    [self  setAlertCtrl];
    
    UIView *uploadPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    UIImageView *uploadimage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [uploadimage setImage:[UIImage imageNamed:@"upload.png"]];
    [uploadPadding addSubview:uploadimage];
    _txtfront.leftViewMode = UITextFieldViewModeAlways;
    _txtfront.leftView = uploadPadding ;
    
    UIView *uploadBackPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    UIImageView *uploadbackimage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [uploadbackimage setImage:[UIImage imageNamed:@"upload.png"]];
    [uploadBackPadding addSubview:uploadbackimage];
    _txtLast.leftViewMode = UITextFieldViewModeAlways;
    _txtLast.leftView = uploadBackPadding ;
    
    
    UIView *issuePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    UIImageView *issueimage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [issueimage setImage:[UIImage imageNamed:@"calender.png"]];
    [issuePadding addSubview:issueimage];
    _txtIssue.leftViewMode = UITextFieldViewModeAlways;
    _txtIssue.leftView = issuePadding ;
    
    
    UIView *expiryPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    UIImageView *expiryimage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [expiryimage setImage:[UIImage imageNamed:@"calender.png.png"]];
    [expiryPadding addSubview:expiryimage];
    _txtExpiry.leftViewMode = UITextFieldViewModeAlways;
    _txtExpiry.leftView = expiryPadding ;

    
    UIView *passportPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *passportimage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [passportimage setImage:[UIImage imageNamed:@"passport.png"]];
    [passportPadding addSubview:passportimage];
    _txtPassport.leftViewMode = UITextFieldViewModeAlways;
    _txtPassport.leftView = passportPadding ;
    
    
    UIView *mobilePadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *mobileImage = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 25, 25)];
    [mobileImage setImage:[UIImage imageNamed:@"mobile.png"]];
    [mobilePadding addSubview:mobileImage];
    _txtLocalMobile.leftViewMode = UITextFieldViewModeAlways;
    _txtLocalMobile.leftView = mobilePadding ;
    
    self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;
     self.btnVerify.layer.cornerRadius = _btnVerify.frame.size.height/2;
    
    
    
    _txtIssue.adjustsFontSizeToFitWidth = YES;
 
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)tapGesture{
    [self.view endEditing:YES];
}

- (IBAction)submitClicked:(id)sender {
    
    if ([self.txtPassport.text isEqualToString:@""] ) {
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your Passport number"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }else if ([self.txtIssue.text isEqualToString:@""] ) {
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your Passport's issue date"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }else if ([self.txtExpiry.text isEqualToString:@""] ) {
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your Passport's expiry date"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else if ([self.txtfront.text isEqualToString:@""] ) {
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please upload Passport's front image"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }else if ([self.txtLast.text isEqualToString:@""] ) {
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please upload Passport's back Image"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else{
         [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
        [self  postRegistrationToServer];
      
    }
}

- (IBAction)resendClicked:(id)sender {
    [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
    [self ResendOtpFromApi];
    
}

- (IBAction)verifyClicked:(id)sender {
    if ([_txtOtp.text isEqualToString:@""]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your OTP number"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }else{
        [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
        [self  verifyOtpFromApi];
        
        
    
    }
    
    
}

-(void)kvnStart{
    [KVNProgress show];
}



-(void)verifyOtpFromApi{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"fhttp://10.10.1.57:8081/wpayWeb/unsecure/usr/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_mobile,
                @"regotp" :registeredOtp,
                @"cnsmrId" :consumerId,
                @"action" : @"Verify"
                
                };
    
    
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
                    verifiedData = responseObject;
         [self performSelectorOnMainThread:@selector(goToPasswordView) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
    
    
    
}

-(void)goToPasswordView {
    
    if ([[verifiedData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        CreatePasswordVc *Cvc = [[CreatePasswordVc alloc]init];
        Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
        [self.navigationController pushViewController:Cvc animated:YES];
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[verifiedData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


- (IBAction)backClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}


///select picture

-(void)setAlertCtrl;
{
    
    self.AlertCtrl = [UIAlertController alertControllerWithTitle:@"selectImage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             
                             {
                                 [self camera];
                                 
                                 isCamera = true;
                                 
                                 
                             }];
    
    UIAlertAction *Library  = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action)
                               
                               {
                                   
                                   [self selectPhoto];
                                   
                                   isCamera = false;
                               }];
    
    UIAlertAction *Cancel  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        isCamera = false;
        
    }];
    
    [self.AlertCtrl addAction:camera];
    [self.AlertCtrl addAction:Library];
    [self.AlertCtrl addAction:Cancel];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

-(void)camera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        
    } else {
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    if ([checkFrontBack isEqualToString:@"Front"]) {
        if (isCamera == true) {
            
            frontImage = info[UIImagePickerControllerEditedImage];
            
            NSData *webData = UIImagePNGRepresentation(frontImage);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            self.txtfront.text = documentsDirectory ;
            UIImageWriteToSavedPhotosAlbum(frontImage, nil, nil, nil);
            
            
            
        }else{
            
            
            frontImage = info[UIImagePickerControllerEditedImage];
            NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
            PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
            NSString *filename = [[result firstObject] filename];
            self.txtfront.text = filename ;
            
            
        }
    }else if ([checkFrontBack isEqualToString:@"Back"]){
        
        if (isCamera == true) {
            
            BackImage = info[UIImagePickerControllerEditedImage];
            
            NSData *webData = UIImagePNGRepresentation(BackImage);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            self.txtLast.text = documentsDirectory ;
            UIImageWriteToSavedPhotosAlbum(BackImage, nil, nil, nil);
            
            
            
        }else{
            
            
            BackImage = info[UIImagePickerControllerEditedImage];
            NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
            PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
            NSString *filename = [[result firstObject] filename];
            self.txtLast.text = filename ;
            
            
        }
        
    }
    
   
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
   
    
    if (textField == _txtfront) {
        
      
        
        isCamera = false ;
        checkFrontBack = @"Front";
        [self presentViewController:self.AlertCtrl animated:YES completion:nil];
        return NO ;
        
        
    }else if (textField == _txtLast) {
       
        isCamera = false ;
        checkFrontBack = @"Back";
        [self presentViewController:self.AlertCtrl animated:YES completion:nil];
        return NO;
    }
    else if (textField == _txtIssue) {
        checkExpiry = @"";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }    else if (textField == _txtExpiry) {
        checkExpiry = @"Expiry";
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 0;
            [self.view layoutIfNeeded];
            
        }];
        
        return NO;
    }
    
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_txtLocalMobile) {
        if (textField.text.length == 10 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
            
        }
    }
    return YES;
    
}
    

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}

-(void)localMobileDidChange:(UITextField *)theTextField{
    
    if (_txtLocalMobile.text.length >0) {
        _txtLocalMobile.placeholder = @"      Local mobile number(Optional)";
        
    }else{
        _txtLocalMobile.placeholder = @"      +91 8556841629";
    }
    
    
}

- (IBAction)doneClicked:(id)sender {
     [self ShowSelectedDate];
}

-(void)ShowSelectedDate{
    
   
    if ([checkExpiry isEqualToString:@"Expiry"]) {
        NSDateFormatter *Df = [[NSDateFormatter alloc]init];
        [Df setDateFormat:@"dd-MM-yyyy"];
       NSString*dateString=[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicked.date]];
        self.txtExpiry.text = dateString ;
        NSDate *dateFromString = [[NSDate alloc] init];
         Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
         dateFromString = [Df dateFromString:dateString];
        NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
        NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
        expiryDate = [dtTime integerValue];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 270;
            [self.view layoutSubviews];
            
        }];
    }
    else
    {
        NSDateFormatter *Df = [[NSDateFormatter alloc]init];
        [Df setDateFormat:@"dd-MM-yyyy"];
        NSString*firstString=[NSString stringWithFormat:@"%@",[Df stringFromDate:self.datePicked.date]];
        self.txtIssue.text = firstString ;
        
        NSDate *dateFromString = [[NSDate alloc] init];
         Df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFromString = [Df dateFromString:firstString];
        NSTimeInterval timeInMiliseconds = [dateFromString timeIntervalSince1970]*1000;
        NSString*dtTime = [NSString stringWithFormat:@"%f",timeInMiliseconds];
        issueDate = [dtTime integerValue];
        [UIView animateWithDuration:0.5 animations:^{
            self.bottomDate.constant = 270;
            [self.view layoutSubviews];
            
        }];
        
    }
}



-(void)postRegistrationToServer {
    
   NSString*issue =[NSString stringWithFormat:@"%ld",issueDate];
      NSString*expiry =[NSString stringWithFormat:@"%ld",expiryDate];
    
    NSString *urlString=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/unsecure/usr/reg?"];
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:_countryName forKey:@"country"];
    [_params setObject:@"1" forKey:@"countryCode"];
    [_params setObject:_firstName forKey:@"firstName"];
    [_params setObject:_middleName forKey:@"middleName"];
    [_params setObject:_lastName forKey:@"lastName"];
    [_params setObject:_gmail forKey:@"email"];
    [_params setObject:_DOB forKey:@"dob"];
    [_params setObject:@"Individual" forKey:@"userType"];
    
    
    [_params setObject:_mobile forKey:@"mobileNo"];
    [_params setObject:@"" forKey:@"aadharNumber"];
    
    [_params setObject:_txtPassport.text forKey:@"passPortNumber"];
    [_params setObject:issue forKey:@"issuePassPortDate"];
    
    [_params setObject:expiry forKey:@"expiryPassportDate"];
    [_params setObject:@"" forKey:@"localMobileNo"];
    [_params setObject:@"IOS" forKey:@"src"];
    
    
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    
    NSString* aadharKey = @"aadharImage";
    NSString*passFrontKey = @"passFrontImage";
    NSString*passBackKey = @"passBackImage" ;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSData *aadhardata = UIImageJPEGRepresentation(aadharImage, 1.0);
    NSData*frontData = UIImageJPEGRepresentation(frontImage, 1.0);
    NSData*BackData = UIImageJPEGRepresentation(BackImage, 1.0);
    if (aadhardata) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", aadharKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:aadhardata];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (frontData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", passFrontKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:frontData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (BackData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", passBackKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:BackData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
                                      
                                      
                                      NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                      
                                      if(data!=nil){
                                          signupData =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
                                          
                                          NSLog(@"%@",signupData);
                                          
                                          [self performSelectorOnMainThread:@selector(successMessage) withObject:nil waitUntilDone:YES];
                                          
                                      }else{
                                          
                                          UIAlertController * alert = [UIAlertController
                                                                       alertControllerWithTitle:nil
                                                                       message:@"Error From Server"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction* noButton = [UIAlertAction
                                                                     actionWithTitle:@"Ok"
                                                                     style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         
                                                                     }];
                                          
                                          [alert addAction:noButton];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                      
                                      
                                      
                                        [self performSelectorOnMainThread:@selector(successMessage) withObject:nil waitUntilDone:YES];
                                      
                                      
                                      NSLog(@"%@",signupData);
                                  }];
    
    [task resume];
    
}

-(void)successMessage{
    
    if ([[signupData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
         _viewOtp.hidden = NO;
        registeredOtp = [signupData valueForKey:@"regotp"];
        consumerId = [signupData valueForKey:@"cnsmrId"];
        _txtOtp.text = registeredOtp ;
        
        
    }else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[signupData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }

}
-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}

-(void)ResendOtpFromApi{
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/unsecure/usr/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_mobile,
                @"regotp" :registeredOtp,
                @"cnsmrId" :consumerId,
                @"action" : @"Resend"
                
                };
    
    
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
                     resendData = responseObject;
         [self performSelectorOnMainThread:@selector(resendOtpResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
    
    
    
}

-(void)resendOtpResponse {
    
    if ([[resendData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
       
            
            NSString*getOtp = [resendData valueForKey:@"otp"];
            _txtOtp.text = [NSString stringWithFormat:@"%@",getOtp] ;
            
            
            NSLog(@"success");
            
            
        }
        
        
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[resendData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }

}

@end
