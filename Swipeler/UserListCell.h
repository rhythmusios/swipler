//
//  UserListCell.h
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *txtSN;
@property (strong, nonatomic) IBOutlet UILabel *txtName;

@end
