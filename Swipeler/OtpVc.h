//
//  OtpVc.h
//  CityBus
//
//  Created by katoch on 19/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtpVc : UIViewController<UIGestureRecognizerDelegate>{
    
    id resendData;
}
- (IBAction)VarifyClicked:(id)sender;
- (IBAction)resendOtpClicked:(id)sender;
- (IBAction)backClicked:(id)sender;



@property (strong, nonatomic) IBOutlet UITextField *txtOtp;
@property (strong, nonatomic) IBOutlet UIButton *btnVarify;
@end
