//
//  AadharVc.m
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import "AadharVc.h"
#import "CreatePasswordVc.h"
#import <QuartzCore/QuartzCore.h>
#import "KVNProgress.h"
@interface AadharVc ()

@end

@implementation AadharVc

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _otpView.hidden = YES;
    _btnVerify.layer.borderWidth = 1.0f;
    _btnVerify.layer.borderColor = [UIColor colorWithRed:245/255.0f green:115/255.0f blue:115/255.0f alpha:1].CGColor;
    
    UITapGestureRecognizer *gesRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)]; // Declare the Gesture.
    gesRecognizer4.delegate = self;
    [self.view addGestureRecognizer:gesRecognizer4];
    
    [self  setAlertCtrl];
    
    UIView *aadharPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *aadharimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [aadharimage setImage:[UIImage imageNamed:@"aadhar-Small@2x.png"]];
    [aadharPadding addSubview:aadharimage];
    _txtAadhar.leftViewMode = UITextFieldViewModeAlways;
    _txtAadhar.leftView = aadharPadding ;
    
    UIView *uploadPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *uploadimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [uploadimage setImage:[UIImage imageNamed:@"mobile.png"]];
    [uploadPadding addSubview:uploadimage];
    _aadharMobile.leftViewMode = UITextFieldViewModeAlways;
    _aadharMobile.leftView = uploadPadding ;
    
    self.btnSubmit.layer.cornerRadius = _btnSubmit.frame.size.height/2 ;
    self.btnVerify.layer.cornerRadius = _btnVerify.frame.size.height/2 ;
   
    [_btnSubmit setEnabled:YES];
    [_btnSubmit setAlpha:1.0];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
    
}
-(void)tapGesture{
    [self.view endEditing:YES];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)submitClicked:(id)sender {
    
     if ([self.txtAadhar.text isEqualToString:@""] && [self.aadharMobile.text isEqualToString:@""])
     {
        
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Please enter the required field"
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
         
         
     }
    
    
   
   else if (![_aadharMobile.text isEqualToString:@""] && [_txtAadhar.text isEqualToString:@""]) {
        
       if (self.aadharMobile.text.length < 10){
           UIAlertController * alert = [UIAlertController
                                        alertControllerWithTitle:nil
                                        message:@"Please enter the registered aadhaar number"
                                        preferredStyle:UIAlertControllerStyleAlert];
           UIAlertAction* noButton = [UIAlertAction
                                      actionWithTitle:@"Ok"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                      }];
           
           [alert addAction:noButton];
           [self presentViewController:alert animated:YES completion:nil];
           
       }
       else{
           
           [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
           [self  postRegistrationToServer];
           
       }
   }
   else if ([_aadharMobile.text isEqualToString:@""] && ![_txtAadhar.text isEqualToString:@""]) {
       
       if (self.txtAadhar.text.length < 12){
           UIAlertController * alert = [UIAlertController
                                        alertControllerWithTitle:nil
                                        message:@"Please enter the valid aadhaar number"
                                        preferredStyle:UIAlertControllerStyleAlert];
           UIAlertAction* noButton = [UIAlertAction
                                      actionWithTitle:@"Ok"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                      }];
           
           [alert addAction:noButton];
           [self presentViewController:alert animated:YES completion:nil];
           
       }else{
           
           [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
           [self  postRegistrationToServer];
           
       }
       
   }else if (self.aadharMobile.text.length < 10){
       UIAlertController * alert = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:@"Please enter the registered aadhaar number"
                                    preferredStyle:UIAlertControllerStyleAlert];
       UIAlertAction* noButton = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      
                                  }];
       
       [alert addAction:noButton];
       [self presentViewController:alert animated:YES completion:nil];
       
   }
   else if (self.txtAadhar.text.length < 12){
       UIAlertController * alert = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:@"Please enter the valid aadhaar number"
                                    preferredStyle:UIAlertControllerStyleAlert];
       UIAlertAction* noButton = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      
                                  }];
       
       [alert addAction:noButton];
       [self presentViewController:alert animated:YES completion:nil];
       
   }
    
    
    
     else{
         [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
         [self  postRegistrationToServer];
         
         

        
     }
    
}


- (IBAction)resendClicked:(id)sender {
    _txtOtp.text = @"";
      [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
    [self ResendOtpFromApi];
}

- (IBAction)verifyClciked:(id)sender {
    if ([_txtOtp.text isEqualToString:@""]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Please enter your OTP number"
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];

        
    }else{
       [self performSelectorOnMainThread:@selector(kvnStart) withObject:nil waitUntilDone:YES];
    [self  verifyOtpFromApi];
    
    }
    
}

-(void)kvnStart{
    [KVNProgress show];
}


-(void)postRegistrationToServer {
    
    
    NSString *urlString=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/unsecure/usr/reg?"];
  
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:_countryName forKey:@"country"];
    [_params setObject:@"91" forKey:@"countryCode"];
    [_params setObject:_firstName forKey:@"firstName"];
    [_params setObject:_middleName forKey:@"middleName"];
    [_params setObject:_lastName forKey:@"lastName"];
    [_params setObject:_gmail forKey:@"email"];
    [_params setObject:_DOB forKey:@"dob"];
    [_params setObject:@"Individual" forKey:@"userType"];


    [_params setObject:_mobile forKey:@"mobileNo"];
    [_params setObject:_txtAadhar.text forKey:@"aadharNumber"];
     [_params setObject:_aadharMobile.text forKey:@"aadharRegistereMobileNo"];
    

    [_params setObject:@"" forKey:@"passPortNumber"];
    [_params setObject:@"" forKey:@"issuePassPortDate"];

    [_params setObject:@"" forKey:@"expiryPassportDate"];
    [_params setObject:@"" forKey:@"localMobileNo"];
    [_params setObject:@"IOS" forKey:@"src"];
    
    
    

    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    
    NSString* aadharKey = @"aadharImage";
    NSString*passFrontKey = @"passFrontImage";
    NSString*passBackKey = @"passBackImage" ;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    NSData *aadhardata = UIImageJPEGRepresentation(aadharImage, 1.0);
    NSData*frontData = UIImageJPEGRepresentation(passportFont, 1.0);
    NSData*BackData = UIImageJPEGRepresentation(passportBack, 1.0);
    if (aadhardata) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", aadharKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:aadhardata];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (frontData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", passFrontKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:frontData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (BackData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", passBackKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:BackData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
                                      
                                      
                                      NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                      
                                      
                                      if(data!=nil){
                                      signupData =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
                                          
                                          NSLog(@"%@",signupData);
                                          
                                  [self performSelectorOnMainThread:@selector(successMessage) withObject:nil waitUntilDone:YES];
                                          
                                      }else{
                                          
                                          UIAlertController * alert = [UIAlertController
                                                                       alertControllerWithTitle:nil
                                                                       message:@"Error From Server"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction* noButton = [UIAlertAction
                                                                     actionWithTitle:@"Ok"
                                                                     style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * action) {
                                                                         
                                                                     }];
                                          
                                          [alert addAction:noButton];
                                          [self presentViewController:alert animated:YES completion:nil];
                                      }
                                      
                                      
                                      
                                      
                                      
                                  }];
    
    [task resume];
    
}

-(void)successMessage{
    
    if ([[signupData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
        
        registeredOtp = [signupData valueForKey:@"regotp"];
        consumerId = [signupData valueForKey:@"cnsmrId"];
        tokenId = [signupData valueForKey:@"token"];
        
        [[NSUserDefaults standardUserDefaults]setObject:tokenId forKey:@"tokenID"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
//        _txtOtp.text = [NSString stringWithFormat:@"%@",registeredOtp] ;
         _otpView.hidden = NO;
        [_btnSubmit setEnabled:NO];
        [_btnSubmit setAlpha:0.6];

    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[signupData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
  

}

-(void)ResendOtpFromApi{
    
  
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/unsecure/otp"];
    
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_mobile,
                @"regotp" :_txtOtp.text,
                @"cnsmrId" :consumerId,
                @"action" : @"Resend"
                
                };
    
    
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"PLIST: %@", responseObject);
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         
                    resendData = responseObject;
         [self performSelectorOnMainThread:@selector(resendOtpResponse) withObject:nil waitUntilDone:YES];
         
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
         NSLog(@"Error: %@", error);
         
         
         
     }];
    
    
    
}

-(void)resendOtpResponse {
    
    if ([[resendData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
       
     NSString*getOtp = [resendData valueForKey:@"otp"];
//        _txtOtp.text = [NSString stringWithFormat:@"%@",getOtp] ;
      NSLog(@"success");
        
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:[resendData valueForKey:@"message"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];

    }
}


-(void)verifyOtpFromApi{
    
    
    
    NSString *apiURLStr=[NSString stringWithFormat:@"http://10.10.1.57:8081/wpayWeb/unsecure/otp"];
    AFHTTPSessionManager * managerr = [AFHTTPSessionManager manager];
    managerr.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary* _params = [[NSDictionary alloc] init];
    
    _params = @{
                @"mobileNo" :_mobile,
                @"regotp" :_txtOtp.text,
                @"cnsmrId" :consumerId,
                @"action" : @"Verify"
                
                };
    
    
    [managerr POST:apiURLStr parameters:_params success:^(NSURLSessionTask *task, id responseObject)
     {
        NSLog(@"PLIST: %@", responseObject);
        [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        
                    verifiedData = responseObject;
        [self performSelectorOnMainThread:@selector(goToPasswordView) withObject:nil waitUntilDone:YES];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
               [self performSelectorOnMainThread:@selector(kvnDismiss) withObject:nil waitUntilDone:YES];
        NSLog(@"Error: %@", error);
        
        
        
    }];
    
    
    
}

-(void)goToPasswordView {
    
     if ([[verifiedData valueForKey:@"responseCode"] isEqual:[NSNumber numberWithInt:200]]){
            CreatePasswordVc *Cvc = [[CreatePasswordVc alloc]init];
            Cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordVc"];
         Cvc.ConsumerID = consumerId;
            [self.navigationController pushViewController:Cvc animated:YES];
         
     }else{
         
         UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:[verifiedData valueForKey:@"message"]
                                      preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction* noButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                    }];
         
         [alert addAction:noButton];
         [self presentViewController:alert animated:YES completion:nil];
     }
}


-(void)kvnDismiss{
    [KVNProgress dismiss];
    
}


///select aadhaar

-(void)setAlertCtrl;
{
    
    self.AlertCtrl = [UIAlertController alertControllerWithTitle:@"selectImage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                             
                             {
                                 [self camera];
                                 
                                 isCamera = true;
                                 
                                 
                             }];
    
    UIAlertAction *Library  = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action)
                               
                               {
                                   
                                   [self selectPhoto];
                                   
                                   isCamera = false;
                               }];
    
    UIAlertAction *Cancel  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *  action) {
        isCamera = false;
        
    }];
    
    [self.AlertCtrl addAction:camera];
    [self.AlertCtrl addAction:Library];
    [self.AlertCtrl addAction:Cancel];
    
}

- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

-(void)camera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        
    } else {
        
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.txtUploadAadhar.text = @"";
   
    
        if (isCamera == true) {
            
            aadharImage = info[UIImagePickerControllerEditedImage];
            
            NSData *webData = UIImagePNGRepresentation(aadharImage);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            self.txtUploadAadhar.text = documentsDirectory ;
            UIImageWriteToSavedPhotosAlbum(aadharImage, nil, nil, nil);
            
            
            
        }else{
            
            
            aadharImage = info[UIImagePickerControllerEditedImage];
            NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
            PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[refURL] options:nil];
            NSString *filename = [[result firstObject] filename];
            self.txtUploadAadhar.text = filename ;
            
            
        }
    
        


    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

//////////////////////////////////////////textfield //////////////////////////////////////


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    
//    if (textField == _txtUploadAadhar) {
//
//        isCamera = false ;
//
//        [self presentViewController:self.AlertCtrl animated:YES completion:nil];
//        return NO ;
//
//
//    }

    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES ;
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField ==_txtAadhar) {
        if (textField.text.length == 12 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
    }
        else if (textField ==_aadharMobile) {
            if (textField.text.length == 10 && range.length == 0)
            {
                return NO; // return NO to not change text
            }
        }
    
    return YES;
    
}



@end
