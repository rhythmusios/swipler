//
//  AadharVc.h
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"
#import <Photos/Photos.h>
#import <AFNetworking/AFNetworking.h>

@interface AadharVc : UIViewController<UITextFieldDelegate,DropDownViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate,UIGestureRecognizerDelegate>{
    
    BOOL isCamera ;
    UIImage*aadharImage;
    id signupData;
    UIImage *passportFont;
    UIImage*passportBack;
    
    NSString*  consumerId ;
    NSString*registeredOtp;
   id verifiedData;
    id resendData ;
    
    NSString* tokenId ;
    
}

@property (strong, nonatomic) IBOutlet UITextField *txtAadhar;
@property (strong, nonatomic) IBOutlet UITextField *aadharMobile;

@property (strong, nonatomic) IBOutlet UITextField *txtUploadAadhar;
@property (strong, nonatomic) IBOutlet UIButton *txtSubmit;
@property (strong, nonatomic) IBOutlet UITextField *txtOtp;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnVerify;
@property (strong, nonatomic) IBOutlet UIAlertController *AlertCtrl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomDate;
@property (strong, nonatomic) IBOutlet UIView *otpView;

@property (strong, nonatomic)NSString*countryName ;
@property (strong, nonatomic)NSString*countryCode ;
@property (strong, nonatomic)NSString*firstName ;
@property (strong, nonatomic)NSString*lastName ;
@property (strong, nonatomic)NSString*middleName ;
@property (strong, nonatomic)NSString*userType ;
@property (strong, nonatomic)NSString*DOB;
@property (strong, nonatomic)NSString*mobile;
@property (strong, nonatomic)NSString*gmail;



- (IBAction)backClicked:(id)sender;

- (IBAction)submitClicked:(id)sender;
- (IBAction)resendClicked:(id)sender;
- (IBAction)verifyClciked:(id)sender;
@end
