//
//  CreatePasswordVc.h
//  CityBus
//
//  Created by katoch on 21/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreatePasswordVc : UIViewController<UIGestureRecognizerDelegate>{
    
    id passwordData;
    NSString* cosumerId ;
}
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (strong, nonatomic)NSString*ConsumerID;

- (IBAction)backClicked:(id)sender;
- (IBAction)submitClicked:(id)sender;
@end
