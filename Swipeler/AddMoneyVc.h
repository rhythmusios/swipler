//
//  AddMoneyVc.h
//  CityBus
//
//  Created by katoch on 26/12/17.
//  Copyright © 2017 Rhythmus Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol senddataProtocol <NSObject>
-(void)sendDataToA:(NSString *)amountStr;
@end

@interface AddMoneyVc : UIViewController<UIGestureRecognizerDelegate>{
    double finalamount;
    id rechargeData;
    id paymentData;
    NSString*consumerId;
}

@property(nonatomic,assign)id delegate;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnHundred;
@property (strong, nonatomic) IBOutlet UIButton *btnTwoHundred;
@property (strong, nonatomic) IBOutlet UIButton *btnFiveHundred;
@property (strong, nonatomic) IBOutlet UIButton *btnThousand;
@property (strong, nonatomic) IBOutlet UIButton *btnAddMoney;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

- (IBAction)hundredClicked:(id)sender;
- (IBAction)twoHundredClicked:(id)sender;
- (IBAction)fiveHundred:(id)sender;
- (IBAction)thousandClicked:(id)sender;
- (IBAction)addMoneyClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
@end
